<?php
namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class UsuarioType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class)
            ->add('email', TextType::class)
            ->add('password', \Symfony\Component\Form\Extension\Core\Type\PasswordType::class,['required'=>false])
            ->add('empresa', EntityType::class,array(
                'class' => 'AppBundle:Empresa',
                'choice_label' => 'sociedad',
                //'property' => array('sociedad','nombre','cif'),
                'required' => true,
                'attr'=>array('class'=>'select2'),
            ))                
            ->add(
                'roles', ChoiceType::class, [
                    'choices' => [
                        'Administrador' => 'ROLE_RG_ADMIN',
                        'MANAGER (Todas las opciones)'=> 'ROLE_RG_MANAGER',
                        '--COMERCIAL'=> 'ROLE_RG_COMERCIAL',
                        '--OPERADOR' => 'ROLE_RG_OPERADOR',
                        '--FINANCIERO' => 'ROLE_RG_FINANCIERO',
                        'Gestión global' => 'ROLE_RG_GLOBAL',
                        'Usuario' => 'ROLE_USER'
                        ],
                    'expanded' => true,
                    'multiple' => true,
                ]
            )
                
            ->add('save', SubmitType::class)
        ;
    }
}