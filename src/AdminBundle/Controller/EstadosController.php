<?php

namespace AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/estados")
 */
class EstadosController extends Controller
{

    /**
     * @Route("/", name="admin_estados")
     */
    public function indexAction(Request $request)
    {
        $eReservas = $this->getDoctrine()->getRepository(\AppBundle\Entity\Estado\Reserva::class)->findAll();
        $eMovimientos = $this->getDoctrine()->getRepository(\AppBundle\Entity\Estado\Movimiento::class)->findAll();
        $eFacturas = $this->getDoctrine()->getRepository(\Iweb\FactuBundle\Entity\Estado::class)->findAll();

        return $this->render('AdminBundle:Estados:listado.html.twig', array(
                    'eReservas' => $eReservas,
                    'eMovimientos' => $eMovimientos,
                    'eFacturas' => $eFacturas,
        ));
    }

    /**
     * @Route("/add", name="admin_estados_add")
     */
    public function addAction(Request $request)
    {
        $usuario = new \AppBundle\Entity\Usuario();
        $form = $this->createForm(\AdminBundle\Form\UsuarioType::class, $usuario);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $usuario = $form->getData();
            $this->getDoctrine()->getManager()->persist($usuario);
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('admin_usuarios');
        }
        return $this->render('@Admin/Usuarios/edit.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/{usuario}", name="admin_usuarios_detalle")
     */
    public function detalleAction(Request $request, \AppBundle\Entity\Usuario $usuario)
    {

    }

    /**
     * @Route("/{usuario}/editar", name="admin_usuarios_editar")
     */
    public function editarAction(Request $request, \AppBundle\Entity\Usuario $usuario)
    {
        $form = $this->createForm(\AdminBundle\Form\UsuarioType::class, $usuario);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $usuario = $form->getData();
            $this->getDoctrine()->getManager()->persist($usuario);
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('admin_usuarios');
        }
        return $this->render('@Admin/Usuarios/edit.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

}
