<?php

namespace AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/usuarios")
 */
class UsuariosController extends Controller
{

    /**
     * @Route("/", name="admin_usuarios")
     */
    public function indexAction(Request $request)
    {
        $usuarios = $this->getDoctrine()->getRepository('AppBundle:Usuario')->findAll();
        return $this->render('AdminBundle:Usuarios:listado.html.twig', array(
                    'usuarios' => $usuarios,
        ));
    }

    /**
     * @Route("/add", name="admin_usuarios_add")
     */
    public function addAction(Request $request)
    {
        $usuario = new \AppBundle\Entity\Usuario();
        $form = $this->createForm(\AdminBundle\Form\UsuarioType::class, $usuario);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $usuario = $form->getData();
            $this->getDoctrine()->getManager()->persist($usuario);
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('admin_usuarios');
        }
        return $this->render('@Admin/Usuarios/edit.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/{usuario}", name="admin_usuarios_detalle")
     */
    public function detalleAction(Request $request, \AppBundle\Entity\Usuario $usuario)
    {

    }

    /**
     * @Route("/{usuario}/delete", name="admin_usuarios_borrar")
     */
    public function deleteAction(Request $request, \AppBundle\Entity\Usuario $usuario)
    {
        
    }

    /**
     * @Route("/{usuario}/editar", name="admin_usuarios_editar")
     */
    public function editarAction(Request $request, \AppBundle\Entity\Usuario $usuario)
    {
        $form = $this->createForm(\AdminBundle\Form\UsuarioType::class, $usuario);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $usuario = $form->getData();
            $this->getDoctrine()->getManager()->persist($usuario);
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('admin_usuarios');
        }
        return $this->render('@Admin/Usuarios/edit.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

}
