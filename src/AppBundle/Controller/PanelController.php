<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/panel")
 */
class PanelController extends Controller
{

    /**
     * @Route("/", name="panel_inicio")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        if (!$this->isGranted('ROLE_RG_OPERADOR')) {
            return $this->redirectToRoute("panel_cliente");
        }

        if ($this->isGranted('ROLE_RG_GLOBAL')) {
            $activas = $em->getRepository(\AppBundle\Entity\Reserva::class)->listarActivas("panel");
            $entregas = $em->getRepository(\AppBundle\Entity\Movimiento::class)->findByTipo("entrega", "panel");
            $recogidas = $em->getRepository(\AppBundle\Entity\Movimiento::class)->findByTipo("recogida", "panel");
            $presupuestos = $em->getRepository(\AppBundle\Entity\Reserva::class)->listarEstado("PRESUPUESTO", "panel");
            $facturas = $em->getRepository(\Iweb\FactuBundle\Entity\Factura::class)->findByEstado("FACTURADA");
        } else {
            $activas = $em->getRepository(\AppBundle\Entity\Reserva::class)->listarActivas("panel", $this->getUser());
            $entregas = $em->getRepository(\AppBundle\Entity\Movimiento::class)->findByTipo("entrega", "panel", $this->getUser());
            $recogidas = $em->getRepository(\AppBundle\Entity\Movimiento::class)->findByTipo("recogida", "panel", $this->getUser());
            $presupuestos = $em->getRepository(\AppBundle\Entity\Reserva::class)->listarEstado("PRESUPUESTO", "panel", $this->getUser());
            $facturas = $em->getRepository(\Iweb\FactuBundle\Entity\Factura::class)->findByEstado("FACTURADA", $this->getUser());
        }


        return $this->render('AppBundle:Default:panel.html.twig', array(
                    'reservasactivas' => $activas,
                    'entregas' => $entregas,
                    'recogidas' => $recogidas,
                    'presupuestos' => $presupuestos,
                    'facturas' => $facturas,
        ));
    }

    /**
     * @Route("/cliente", name="panel_cliente")
     */
    public function clienteAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $diarias = $em->getRepository(\AppBundle\Entity\Reserva::class)->listarDiarias($this->getUser());
        $proximas = $em->getRepository(\AppBundle\Entity\Reserva::class)->listarProximas($this->getUser());
        $activas = $em->getRepository(\AppBundle\Entity\Reserva::class)->listarActivas("panel", $this->getUser());
        $presupuestos = $em->getRepository(\AppBundle\Entity\Reserva::class)->listarEstado("PRESUPUESTO", "panel", $this->getUser());

        return $this->render('AppBundle:Clientes:panel.html.twig', array(
                    'diarias' => $diarias,
                    'proximas' => $proximas,
                    'activas' => $activas,
                    'presupuestos' => $presupuestos,
        ));
    }

    /**
     * @Route("/pdf", name="panel_inicio_pdf")
     */
    public function indexPdfAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        if ($this->isGranted('ROLE_RG_GLOBAL')) {
            $activas = $em->getRepository(\AppBundle\Entity\Reserva::class)->listarActivas("panel");
            $entregas = $em->getRepository(\AppBundle\Entity\Movimiento::class)->findByTipo("entrega", "panel");
            $recogidas = $em->getRepository(\AppBundle\Entity\Movimiento::class)->findByTipo("recogida", "panel");
        } else {
            $activas = $em->getRepository(\AppBundle\Entity\Reserva::class)->listarActivas("panel", $this->getUser());
            $entregas = $em->getRepository(\AppBundle\Entity\Movimiento::class)->findByTipo("entrega", "panel", $this->getUser());
            $recogidas = $em->getRepository(\AppBundle\Entity\Movimiento::class)->findByTipo("recogida", "panel", $this->getUser());
        }


        $html = $this->renderView('AppBundle:PDF:panel.html.twig', array(
            'reservasactivas' => $activas,
            'entregas' => $entregas,
            'recogidas' => $recogidas
        ));
        $snappy = $this->get('knp_snappy.pdf');
//            $snappy->setOption('header-html', $this->container->get('templating.helper.assets')->getUrl('bundles/core/pdfHtml/header.html'));
//            $snappy->setOption('footer-html', $this->container->get('templating.helper.assets')->getUrl('bundles/core/pdfHtml/footer.html'));
        $snappy->setOption('orientation', 'Landscape');
        $snappy->setOption('footer-right', 'Página [page] de [topage] - ' . date('\ d.m.Y\ H:i'));
        $snappy->setOption('footer-left', 'RGSpain');
        return new Response(
                $snappy->getOutputFromHtml($html), 200, array(
            'Content-Type' => 'application/pdf',
                // 'Content-Disposition'   => 'attachment; filename="RGSpain_Presupuesto_R'.$reserva->getId().'.pdf"'
                )
        );
    }

    /**
     * @Route("/listado/movimientos", name="listado_movimientos")
     */
    public function listadoMovimientosAction()
    {
        return $this->render('AppBundle:Default:movimientos.html.twig', array(
                    'entregas' => $this->listarEntregas(),
                    'recogidas' => $this->listarRecogidas()
        ));
    }

    /**
     * @Route("/formtest", name="panel_test")
     */
    public function newAction(Request $request)
    {




        // createFormBuilder is a shortcut to get the "form factory"
        // and then call "createBuilder()" on it
        $defaults = array(
                //'dueDate' => new \DateTime('tomorrow'),
        );
        $form = $this->createFormBuilder()
                ->add('task', TextType::class)
                ->add('dueDate', 'sonata_type_model_list')
                ->getForm();

        $request = Request::createFromGlobals();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();

            // ... perform some action, such as saving the data to the database

            $response = new RedirectResponse('/task/success');
            $response->prepare($request);

            return $response->send();
        }

        return $this->render('AppBundle:Default:test.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/calendario", name="calendario")
     */
    public function calendarioAction(Request $request)
    {
        $reservas = $this->getDoctrine()->getRepository('AppBundle:Reserva')->listarActivas("panel");

        return $this->render('AppBundle:Default:calendario.html.twig', array(
                    'reservas' => $reservas
        ));
    }

}
