<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Direccion;
use AppBundle\Form\DireccionType;

/**
 * Direccion controller.
 *
 * @Route("/admin/direcciones")
 */
class DireccionController extends Controller
{

    /**
     * Lists all Direccion entities.
     *
     * @Route("/", name="direcciones")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $direccions = $em->getRepository('AppBundle:Direccion')->findAll();

        return $this->render('@App/Direccion/listado.html.twig', array(
                    'direcciones' => $direccions,
        ));
    }

    /**
     * Creates a new Direccion entity.
     *
     * @Route("/new", name="direccion_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $direccion = new Direccion();
        $form = $this->createForm('AppBundle\Form\DireccionType', $direccion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($direccion);
            $em->flush();

            return $this->redirectToRoute('direcciones_show', array('direccion' => $direccion->getId()));
        }

        return $this->render('@App/Direccion/edit.html.twig', array(
                    'direccion' => $direccion,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Direccion entity.
     *
     * @Route("/{direccion}", name="direcciones_show")
     * @Method("GET")
     */
    public function showAction(Direccion $direccion)
    {
        $deleteForm = $this->createDeleteForm($direccion);

        return $this->render('@App/Direccion/show.html.twig', array(
                    'direccion' => $direccion,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Direccion entity.
     *
     * @Route("/{direccion}/edit", name="direcciones_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Direccion $direccion)
    {
        $deleteForm = $this->createDeleteForm($direccion);
        $editForm = $this->createForm('AppBundle\Form\DireccionType', $direccion);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($direccion);
            $em->flush();
            return $this->redirectToRoute ('direcciones_show', array('direccion' => $direccion->getId()));
        }

        return $this->render('@App/Direccion/edit.html.twig', array(
                    'direccion' => $direccion,
                    'form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Direccion entity.
     *
     * @Route("/{direccion}", name="direcciones_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Direccion $direccion)
    {
        $form = $this->createDeleteForm($direccion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($direccion);
            $em->flush();
        }

        return $this->redirectToRoute('direcciones');
    }

    /**
     * Creates a form to delete a Direccion entity.
     *
     * @param Direccion $direccion The Direccion entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Direccion $direccion)
    {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('direcciones_delete', array('direccion' => $direccion->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

}
