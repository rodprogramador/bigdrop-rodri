<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Empresa;
use AppBundle\Form\EmpresaType;
use AppBundle\Entity\DetalleConcepto;
use AppBundle\Form\DetalleConceptoType;
use AppBundle\Entity\Concepto;
use AppBundle\Form\ConceptoType;
use Symfony\Component\HttpFoundation\Response;


class DetalleConceptoController extends Controller
{
  
/**
     * Creates a new DetalleConcepto entity.
     *
     * @Route("/new", name="gastos_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $detalle = new Detalleconcepto();
       // $concepto->setComercial($this->getUser());

        if (!$this->isGranted('ROLE_RG_COMERCIAL')) {
            return $this->redirectToRoute("concepto_new_gasto", ['empresa' => $this->getUser()->getEmpresa()->getId()]);
        }
        $form = $this->createForm('AppBundle\Form\DetalleConceptoType', $detalle);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($detalle);

          
            $em->flush();

            return $this->redirectToRoute('concepto_show', array('Detalleconcepto' => $detalle->getId()));
        }

        return $this->render('AppBundle:DetalleConcepto:new.html.twig', array(
                    'Detalleconcepto' => $detalle,
                    'form' => $form->createView(),
        ));
    }
  /**
     * Finds and displays a DetalleConcepto entity.
     *
     * @Route("/recibidas", name="concepto_show")
     * @Method("GET")
     */
    public function showAction( Concepto $concepto = null)
    {
        $em = $this->getDoctrine()->getManager();

        $queryBuilder = $this->getDoctrine()->getManager()->createQueryBuilder();
        $query = $queryBuilder

              
                       ->select('r')
                       ->from(\AppBundle\Entity\Concepto::class, 'r')
                                   ;
   
        $concepto = $query->getQuery()->getResult();

        return $this->render('@App/DetalleConcepto/show.html.twig', array(
                    
                    'DetalleConcepto' => $concepto,
                    'concepto' => $concepto,

                    'titulotabla' => 'Conceptos',
                ));
    
    }
    
    /**
     * Finds and displays a DetalleConcepto entity.
     *
     * @Route("/recibidas/{id}", name="listado_show")
     * @Method("GET")
     
     */
    public function ListadoAction( $id)
    {
     
        $queryBuilder = $this->getDoctrine()->getManager()->createQueryBuilder();
        $query = $queryBuilder
                ->select('r')
                ->from('AppBundle:DetalleConcepto', 'r')
                
                ->where("r.detalle='$id'");
             
               
              

              

        $detalleconcepto = $query->getQuery()->getResult();

        return $this->render('@App/DetalleConcepto/listado.html.twig', array(
                    'DetalleConcepto' => $detalleconcepto,
                  
                ));

    
            }

            public function generarConcepto($idcliente, $detalle, $fecha = null)
    {
        
        if (is_null($fecha)) {
            $fecha = date("Y-m-d");
        }
        $concepto = new \AppBundle\Entity\Concepto();
        $concepto->set($EmprePropia);
        $concepto->setReceptor($Empresa);
        $concepto->setFechaEmision($fecha);
        $em = $this->getDoctrine()->getManager();
        foreach ($detalle as $idDetalleConcepto) {
            /* @var $DetalleConcepto \AppBundle\Entity\DetalleConcepto */
            $DetalleConcepto = $em->getRepository(\AppBundle\Entity\DetalleConcepto::class)->find($idDetalleConcepto);
            
            
            
            $DetalleConcepto->setconcepto($concepto);
            $concepto->addDetalle($DetalleConcepto);
            $em->persist($DetalleConcepto);
        }
        $concepto>setNumero(($ultima));
        $em->persist($Concepto);
        $em->flush();
        return $Concepto->getId();
    }

    /**
     * @Route("/recibidas", name="concepto_show")
     */
    public function indexAction(Request $request)
    {
        $trimestre = $request->query->get('trimestre');
        $ano = $request->query->get('ano');
        $cliente = $request->query->get('cliente');
        $queryBuilder = $this->getDoctrine()->getManager()->createQueryBuilder();
        $query = $queryBuilder
                ->select('f')
                ->from('AppBundle:Concepto', 'f');

        if (!$this->isGranted('ROLE_RG_GLOBAL')) {
            $query->where("f.receptor = :empresa");
            if ($this->isGranted('ROLE_RG_FINANCIERO')) {
                $query->where("f.emisor = :empresa");
            }
            $query->setParameter("empresa", $this->getUser()->getEmpresa());
        }

        if (!is_null($ano) && strlen($ano) == 4) {
            switch ($trimestre) {
                case '1T':
                    $fini = \DateTime::createFromFormat("d-m-Y", "00-01-" . $ano);
                    $ffin = \DateTime::createFromFormat("d-m-Y", "31-03-" . $ano);
                    break;
                case '2T':
                    $fini = \DateTime::createFromFormat("d-m-Y", "00-04-" . $ano);
                    $ffin = \DateTime::createFromFormat("d-m-Y", "30-06-" . $ano);
                    break;
                case '3T':
                    $fini = \DateTime::createFromFormat("d-m-Y", "00-07-" . $ano);
                    $ffin = \DateTime::createFromFormat("d-m-Y", "30-09-" . $ano);
                    break;
                case '4T':
                    $fini = \DateTime::createFromFormat("d-m-Y", "00-10-" . $ano);
                    $ffin = \DateTime::createFromFormat("d-m-Y", "31-12-" . $ano);
                    break;
                default:
                    $fini = \DateTime::createFromFormat("d-m-Y", "01-01-" . $ano);
                    $ffin = \DateTime::createFromFormat("d-m-Y", "31-12-" . $ano);
                    break;
            }
            $query->andwhere('f.fecha >= :fini AND f.fecha <= :ffin');
            $query->setParameter("fini", $fini)
                    ->setParameter("ffin", $ffin);
        }
  
        $concepto = $query->getQuery()->getResult();

        $empresas = $this->getDoctrine()->getRepository('AppBundle:Empresa')->findAll();

        return $this->render('@App/DetalleConcepto/show.html.twig', array('concepto' => $concepto, 'empresas' => $empresas, "filtro" => array(
                        "ano" => $ano,
                        "trimestre" => $trimestre,
                        "empresa" => $cliente
                    ))
        );
    }

    
}