<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Empresa;
use AppBundle\Form\EmpresaType;
use AppBundle\Entity\Concepto;
use AppBundle\Form\ConceptoType;
use AppBundle\Entity\DetalleConcepto;
use AppBundle\Form\DetalleConceptoType;
use Symfony\Component\HttpFoundation\Response;


class ConceptoController extends Controller
{
    /**
     * Creates a new Concepto entity.
     *
     * @Route("/new", name="gastos_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $concepto = new Concepto();
       // $concepto->setComercial($this->getUser());

        if (!$this->isGranted('ROLE_RG_COMERCIAL')) {
            return $this->redirectToRoute("concepto_new_gasto", ['empresa' => $this->getUser()->getEmpresa()->getId()]);
        }
        $form = $this->createForm('AppBundle\Form\ConceptoType', $concepto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($concepto);

          
            $em->flush();

            return $this->redirectToRoute('concepto_show', array('concepto' => $concepto->getId()));
        }

        return $this->render('AppBundle:concepto:new.html.twig', array(
                    'concepto' => $concepto,
                    'form' => $form->createView(),
        ));
    }
  /**
     * Finds and displays a Concepto entity.
     *
     * @Route("/recibidas", name="concepto_show")
     * @Method("GET")
     */
    public function showAction(Concepto $concepto = null, DetalleConcepto $detalle = null)
    {
        $em = $this->getDoctrine()->getManager();

        $queryBuilder = $this->getDoctrine()->getManager()->createQueryBuilder();
        $query = $queryBuilder

              
                       ->select('r')
                       ->from(\AppBundle\Entity\Concepto::class, 'r')
                      
                      

                      
               ;
   
              
        
          
       
        $concepto = $query->getQuery()->getResult();

        return $this->render('@App/Concepto/show.html.twig', array(
                    
                    'concepto' => $concepto,
                    'DetalleConcepto' => $concepto,

                    'titulotabla' => 'Conceptos',
                ));
         
   

              



    
    
    }
    
}

  

