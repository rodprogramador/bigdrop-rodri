<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Empresa;
use AppBundle\Form\EmpresaType;
use AppBundle\Entity\Reserva;
use AppBundle\Form\ReservaType;
use Symfony\Component\HttpFoundation\Response;






/**
 * Empresa controller.
 *
 * @Route("/admin/empresas")
 */
class EmpresaController extends Controller
{

    protected $em;

    public function __construct(EntityManager $em = null)
    {
        if (!is_null($em)) {
            $this->em = $em;
        }
    }

    protected function em()
    {
        if (is_null($this->em)) {
            $this->em = $this->getDoctrine()->getManager();
        }
        return $this->em;
    }


    /**
     *
     * @Route("/", name="empresas")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $empresas = $em->getRepository('AppBundle:Empresa')->findAll();

        return $this->render('@App/Empresa/listado.html.twig', array(
                    'empresas' => $empresas,
        ));
    }

    /**
     * Creates a new Empresa entity.
     *
     * @Route("/new", name="empresa_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $empresa = new Empresa();
        $form = $this->createForm('AppBundle\Form\EmpresaType', $empresa);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($empresa);
            $em->flush();

            return $this->redirectToRoute('empresa_show', array('empresa' => $empresa->getId()));
        }

        return $this->render('@App/Empresa/edit.html.twig', array(
                    'empresa' => $empresa,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Empresa entity.
     *
     * @Route("/{empresa}", name="empresa_show")
     * @Method("GET")
     */
    public function showAction(Request $request, Empresa $empresa)
    {
       

        $deleteForm = $this->createDeleteForm($empresa);
        $queryBuilder = $this->getDoctrine()->getManager()->createQueryBuilder();
        $query = $queryBuilder
                ->select('r')
                ->from('AppBundle:Reserva', 'r');   
                //->where('r.empresa =  ')
               // $filtro = $this->applyFilters($empresa, $query, false, $this->em()->getRepository(\AppBundle\Entity\Estado\Reserva::class)->findOneByNombre("PRESUPUESTO"));
              
               $filtro = $this->applyFilters($request, $query);

              

        $reservas = $query->getQuery()->getResult();

        return $this->render('@App/Empresa/show.html.twig', array(
                    'empresa' => $empresa,
                    'delete_form' => $deleteForm->createView(),
                    'reservas' => $reservas,
                    'titulotabla' => 'Reservas',
                    'title' => 'Reservas-Listado',
                    'filtro'=> $filtro,
                ));

      

       
    }


  


    /**
     * Displays a form to edit an existing Empresa entity.
     *
     * @Route("/{empresa}/edit", name="empresa_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Empresa $empresa)
    {
        $deleteForm = $this->createDeleteForm($empresa);
        $editForm = $this->createForm('AppBundle\Form\EmpresaType', $empresa);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($empresa);
            $em->flush();
            return $this->redirectToRoute('empresa_show', array('empresa' => $empresa->getId()));
        }

        return $this->render('@App/Empresa/edit.html.twig', array(
                    'empresa' => $empresa,
                    'form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Empresa entity.
     *
     * @Route("/{empresa}", name="empresa_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Empresa $empresa)
    {
        $form = $this->createDeleteForm($empresa);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($empresa);
            $em->flush();
        }

        return $this->redirectToRoute('empresas');
    }

    /**
     * Creates a form to delete a Empresa entity.
     *
     * @param Empresa $empresa The Empresa entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Empresa $empresa)
    {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('empresa_delete', array('empresa' => $empresa->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }
 
    public function applyFilters(Request $request, \Doctrine\ORM\QueryBuilder &$query, $lduracion = false, \AppBundle\Entity\Estado\Reserva $estado = null)
    {
        $inicio = $request->query->get('inicio');
        $fin = $request->query->get('fin');
        $estado = (is_null($estado)) ? $request->query->get('estado') : $estado;
        $modelo = $request->query->get('modelo');
        $lduracion = ($lduracion) ? 'on' : $request->query->get('lduracion');

        if (!is_null($inicio) && !empty($inicio)) {
            $query->andWhere("r.fechaInicio >= :fini ");
            $query->setParameter("fini", $inicio);
        }
        if (!is_null($fin) && !empty($fin)) {
            $query->andWhere("r.fechaFin <= :ffin ");
            $query->setParameter("ffin", $fin);
        }
        if (!is_null($modelo) && !empty($modelo)) {
            $query->andWhere("r.modelo = :modelo ");
            $query->setParameter("modelo", $modelo);
        }
        if ($lduracion != 'on') {
            $query->andWhere("r.fechaFin <= DATE_ADD(r.fechaInicio, 28, 'DAY' )");
        }
       

        if (!is_null($estado) && !empty($estado)) {
            if (!$estado instanceof \AppBundle\Entity\Estado\Reserva) {
                $estado = $this->em()->getRepository(\AppBundle\Entity\Estado\Reserva::class)->find($estado);
            }
            $query->andWhere("r.estado = :estado ");
            $query->setParameter("estado", $estado);
        }

        if (!$this->isGranted('ROLE_RG_GLOBAL')) {
            $query->leftJoin("r.comercial", "c")
                    ->andWhere('c.empresa = :empresa OR r.empresa = :empresa')
                    ->setParameter('empresa', $this->getUser()->getEmpresa());
        }


        if (!$this->isGranted('ROLE_RG_GLOBAL')) {
            $empresas = [$this->getUser()->getEmpresa()];
        } else {
            $empresas = $this->em()->getRepository(\AppBundle\Entity\Empresa::class)->findAll();
        }
        $estados = $this->em()->getRepository(\AppBundle\Entity\Estado\Reserva::class)->findAll();

        return array(
            'opciones' => [
                'estados' => $estados,
               
            ],
            'valores' => array(
                'inicio' => $inicio,
                'fin' => $fin,
                'estado' => $estado,
                'modelo' => $modelo,
               
                'lduracion' => $lduracion
            )
        );
    }

}
