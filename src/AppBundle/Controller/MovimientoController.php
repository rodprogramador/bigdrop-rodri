<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Movimiento;
use AppBundle\Form\MovimientoType;

/**
 * Movimiento controller.
 *
 * @Route("/panel/movimiento")
 */
class MovimientoController extends Controller
{
    /**
     * Lists all Movimiento entities.
     *
     * @Route("/", name="movimiento_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $movimientos = $em->getRepository('AppBundle:Movimiento')->findAll();

        return $this->render('AppBundle:movimiento:index.html.twig', array(
            'movimientos' => $movimientos,
        ));
    }

    /**
     * Creates a new Movimiento entity.
     *
     * @Route("/new", name="movimiento_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $movimiento = new Movimiento();
        $form = $this->createForm('AppBundle\Form\MovimientoType', $movimiento);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($movimiento);
            $em->flush();

            return $this->redirectToRoute('movimiento_show', array('id' => $movimiento->getId()));
        }

        return $this->render('AppBundle:movimiento:new.html.twig', array(
            'movimiento' => $movimiento,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Movimiento entity.
     *
     * @Route("/{movimiento}", name="movimiento_show")
     * @Method({"GET", "POST"})
     */
    public function showAction(Request $request,Movimiento $movimiento)
    {

        if($request->request->get('form')!==null){
            $em = $this->getDoctrine()->getManager();
            $estado = $this->getDoctrine()->getRepository('AppBundle:Estado')->find($request->request->get('form')['estado']);
            $movimiento->setEstado($estado);
            $em->persist($movimiento);
            $em->flush();
        }

        $estadosForm = $this->createEstadoForm($movimiento);
        
        $criteria = new \Doctrine\Common\Collections\Criteria();
        $criteria
          ->orWhere($criteria->expr()->contains('entrega', $movimiento))
          ->orWhere($criteria->expr()->contains('recogida', $movimiento));
        
        $reservas_entrega = $this->getDoctrine()->getRepository('AppBundle:Reserva')->findByEntrega($movimiento);
        $reservas_recogida = $this->getDoctrine()->getRepository('AppBundle:Reserva')->findByRecogida($movimiento);

        return $this->render('AppBundle:movimiento:show.html.twig', array(
            'movimiento' => $movimiento,
            'estado_form' => $estadosForm->createView(),
            'reservas_entrega' => $reservas_entrega,
            'reservas_recogida' => $reservas_recogida,
            //'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Movimiento entity.
     *
     * @Route("/{id}/edit", name="movimiento_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Movimiento $movimiento)
    {
        $deleteForm = $this->createDeleteForm($movimiento);
        $editForm = $this->createForm('AppBundle\Form\MovimientoType', $movimiento);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($movimiento);
            $em->flush();

            return $this->redirectToRoute('movimiento_show', array('id' => $movimiento->getId()));
        }

        return $this->render('AppBundle:movimiento:edit.html.twig', array(
            'movimiento' => $movimiento,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Movimiento entity.
     *
     * @Route("/{id}", name="movimiento_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Movimiento $movimiento)
    {
        $form = $this->createDeleteForm($movimiento);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($movimiento);
            $em->flush();
        }

        return $this->redirectToRoute('movimiento_index');
    }

    /**
     * Creates a form to delete a Movimiento entity.
     *
     * @param Movimiento $movimiento The Movimiento entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Movimiento $movimiento)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('movimiento_delete', array('id' => $movimiento->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
    
    private function createEstadoForm(Movimiento $movimiento){
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('movimiento_show', array('id' => $movimiento->getId())))
            ->add('estado','entity',array(
                'class' => 'AppBundle\Entity\Estado',
                    'property' => 'nombre', 
                    'query_builder' => function($estado) {
                        return $estado->createQueryBuilder('estado')
                                ->where('estado.ambito = :estado ')
                                ->setParameter(":estado","movimiento");
//                                  ->orderBy('country.name', 'ASC');
                    },
                    'required' => true, 
                    'empty_value' => false))
            ->setMethod('POST')
            ->getForm()
        ;
    }
}
