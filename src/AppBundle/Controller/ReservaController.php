<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Reserva;
use AppBundle\Form\ReservaType;
use Symfony\Component\HttpFoundation\Response;

/**
 * Reserva controller.
 *
 * @Route("/reserva")
 */
class ReservaController extends Controller
{

    /**
     * Creates a new Reserva entity.
     *
     * @Route("/new", name="reserva_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $reserva = new Reserva();
        $reserva->setComercial($this->getUser());

        if (!$this->isGranted('ROLE_RG_COMERCIAL')) {
            return $this->redirectToRoute("reserva_new_cliente", ['empresa' => $this->getUser()->getEmpresa()->getId()]);
        }
        $form = $this->createForm('AppBundle\Form\ReservaType', $reserva);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $estadopresupuesto = $em->getRepository(\AppBundle\Entity\Estado\Reserva::class)->findOneBy(['nombre' => 'PRESUPUESTO']);
            $reserva->setEstado($estadopresupuesto);
            $em->persist($reserva);

            $reserva->getEntrega()->setReserva($reserva);
            $reserva->getRecogida()->setReserva($reserva);
            $em->persist($reserva->getEntrega());
            $em->persist($reserva->getRecogida());
            $em->flush();

            return $this->redirectToRoute('reserva_show', array('reserva' => $reserva->getId()));
        }

        return $this->render('AppBundle:Reserva:new.html.twig', array(
                    'reserva' => $reserva,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a new Reserva entity.
     *
     * @Route("/new/{empresa}", name="reserva_new_cliente")
     * @Method({"GET", "POST"})
     */
    public function newEmpresaAction(Request $request, \AppBundle\Entity\Empresa $empresa = null)
    {
        $reserva = new Reserva();
        $reserva->setComercial($this->getUser());
        if (!is_null($empresa)) {
            $reserva->setEmpresa($empresa);
        }

        $form = $this->createForm('AppBundle\Form\ReservaEmpresaType', $reserva);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $estadopresupuesto = $em->getRepository(\AppBundle\Entity\Estado\Reserva::class)->findOneBy(['nombre' => 'PRESUPUESTO']);
            $reserva->setEstado($estadopresupuesto);

            $interval = new \DateInterval("P1D");
            $est_mov = $em->getRepository(\AppBundle\Entity\Estado\Movimiento::class)->findOneByNombre("EN PREPARACION");

            $f_entrega = clone $reserva->getFechaInicio();
            $f_entrega->sub($interval);
            $f_recogida = clone $reserva->getFechaFin();
            $f_recogida->add($interval);

            $entrega = new \AppBundle\Entity\Movimiento();
            $entrega->setDireccion($empresa->getDirEntrega());
            $entrega->setFecha($f_entrega);
            $entrega->setEstado($est_mov);
            $entrega->setTipo("entrega");
            $em->persist($entrega);
            $reserva->setEntrega($entrega);

            $recogida = new \AppBundle\Entity\Movimiento();
            $recogida->setDireccion($empresa->getDirEntrega());
            $recogida->setFecha($f_recogida);
            $recogida->setEstado($est_mov);
            $recogida->setTipo("recogida");
            $em->persist($recogida);
            $reserva->setRecogida($recogida);

            $em->persist($reserva);
            $em->flush();

            return $this->redirectToRoute('reserva_show', array('reserva' => $reserva->getId()));
        }

        return $this->render('AppBundle:Reserva:new_cliente.html.twig', array(
                    'reserva' => $reserva,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Reserva entity.
     *
     * @Route("/{reserva}", name="reserva_show")
     * @Method("GET")
     */
    public function showAction(Reserva $reserva = null)
    {
        if (is_null($reserva)) {
            return $this->redirectToRoute("panel_inicio");
        }
        if (!$this->isGranted("ROLE_RG_GLOBAL") && $reserva->getEmpresa() != $this->getUser()->getEmpresa() && $reserva->getComercial() != $this->getUser()->getEmpresa()) {
            return $this->redirectToRoute("panel_inicio");
        }

        $em = $this->getDoctrine()->getManager();

        $estadoMovimientos = $em->getRepository("AppBundle:Estado\Movimiento")->findAll();

        if ($reserva->getFactura() === null) {
            $estadoReserva = $em->getRepository(\AppBundle\Entity\Estado\Reserva::class)->findAll();
        } else {
            $query = $em->createQueryBuilder()
                    ->select('e')
                    ->from(\AppBundle\Entity\Estado\Reserva::class, 'e')
                    ->andwhere("e.nombre != 'PRESUPUESTO'")
                    ->andwhere("e.nombre != 'CANCELADO'")
            ;
            $estadoReserva = $query->getQuery()->getResult();
        }
        return $this->render('AppBundle:Reserva:show.html.twig', array(
                    'reserva' => $reserva,
                    'estadosMovimiento' => $estadoMovimientos,
                    'estadosReserva' => $estadoReserva,
//            'edit_form' => $editForm->createView(),
                    'delete_form' => $this->createDeleteForm($reserva)->createView(),
        ));
    }

    /**
     * Finds and displays a Reserva entity.
     *
     * @Route("/{id}", name="reserva_show_update")
     * @Method("POST")
     */
    public function showUpdateAction(Request $request, Reserva $reserva)
    {
        if ($this->isGranted("ROLE_RG_OPERADOR")) {
            $estado = $request->request->get("estado");
            $entregaEstado = $request->request->get("entregaEstado");
            $recogidaEstado = $request->request->get("recogidaEstado");
        }
        $em = $this->getDoctrine()->getManager();
        $repoEstado = $this->getDoctrine()->getRepository(\AppBundle\Entity\Estado\Reserva::class);
        $repoMov = $this->getDoctrine()->getRepository(\AppBundle\Entity\Estado\Movimiento::class);
        if (isset($estado) && !is_null($estado)) {
            $ne = $repoEstado->find($estado);
            if($ne->getNombre() == "FINALIZADA"){
                if($reserva->getFactura() === null){
                    $this->addFlash('warning','No puedes finalizar una reserva sin facturar.');
                    $ne = $reserva->getEstado();
                }else{
                    $em_fin = $repoMov->findOneByNombre("FINALIZADO");
                    $reserva->getEntrega()->setEstado($em_fin);
                    $reserva->getRecogida()->setEstado($em_fin);
                    $entregaEstado = $recogidaEstado = null;
                }
            }
            if($ne->getNombre() == "CANCELADO"){
                $em_fin = $repoMov->findOneByNombre("FINALIZADO");

                $reserva->getEntrega()->setEstado($em_fin);
                $reserva->getRecogida()->setEstado($em_fin);
                $entregaEstado = $recogidaEstado = null;
            }
            
            $reserva->setEstado($ne);
        }
        if (isset($entregaEstado) && !is_null($entregaEstado)) {
            $reserva->getEntrega()->setEstado($repoMov->find($entregaEstado));
        }
        if (isset($recogidaEstado) && !is_null($recogidaEstado)) {
            $reserva->getRecogida()->setEstado($repoMov->find($recogidaEstado));
        }
        
        if($reserva->getEntrega()->getEstado()->getNombre() == "FINALIZADO" && $reserva->getRecogida()->getEstado()->getNombre() == "FINALIZADO" 
        && $ne <> "CANCELADO" 
        ){
            $erFinalizada = $em->getRepository(\AppBundle\Entity\Estado\Reserva::class)->findOneByNombre("FINALIZADA");
            $reserva->setEstado($erFinalizada);
        }
        
        $reserva->setNotas($request->request->get("notas"));
        $em->persist($reserva);
        $em->flush();
        
        $this->addFlash(
            'success',
            'Los cambios han sido guardados'
        );
        
        return $this->redirectToRoute("reserva_show", ["reserva" => $reserva->getId()]);
    }

    /**
     * @Route("/{reserva}/edit", name="reserva_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Reserva $reserva)
    {
        if (!$this->isGranted("ROLE_RG_GLOBAL") && $reserva->getEmpresa() != $this->getUser()->getEmpresa() && $reserva->getComercial() != $this->getUser()->getEmpresa()) {
            return $this->redirectToRoute("panel_inicio");
        }

        if ($reserva->getEmpresa() == $this->getUser()->getEmpresa()) {
            return $this->redirectToRoute("reserva_edit_owner", ['reserva' => $reserva->getId()]);
        }

        if (!$this->isGranted("ROLE_RG_OPERADOR") && $reserva->getFechaInicio() <= new \DateTime("+2 day")) {
            return $this->redirectToRoute("reserva_show", ['reserva' => $reserva->getId()]);
        }
        $deleteForm = $this->createDeleteForm($reserva);
        $editForm = $this->createForm('AppBundle\Form\ReservaType', $reserva);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($reserva);
            $em->flush();
            return $this->redirectToRoute('reserva_show', array('reserva' => $reserva->getId()));
        }

        return $this->render('AppBundle:Reserva:edit.html.twig', array(
                    'reserva' => $reserva,
                    'form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @Route("/{reserva}/editown", name="reserva_edit_owner")
     * @Method({"GET", "POST"})
     */
    public function editOwnAction(Request $request, Reserva $reserva)
    {
        if ($reserva->getEmpresa() != $this->getUser()->getEmpresa()) {
            return $this->redirectToRoute("panel_inicio");
        }

        if (!$this->isGranted("ROLE_RG_OPERADOR") && $reserva->getFechaInicio() <= new \DateTime("+2 day")) {
            return $this->redirectToRoute("reserva_show", ['reserva' => $reserva->getId()]);
        }
        $deleteForm = $this->createDeleteForm($reserva);
        $editForm = $this->createForm(\AppBundle\Form\ReservaEmpresaType::class, $reserva);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($reserva);
            $em->flush();
            return $this->redirectToRoute('reserva_show', array('reserva' => $reserva->getId()));
        }

        return $this->render('AppBundle:Reserva:new_cliente.html.twig', array(
                    'reserva' => $reserva,
                    'form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Reserva entity.
     *
     * @Route("/{id}/delete", name="reserva_delete")
     */
    public function deleteAction(Reserva $reserva)
    {
        if (!$this->isGranted("ROLE_RG_GLOBAL") && $reserva->getEmpresa() != $this->getUser()->getEmpresa() && $reserva->getComercial() != $this->getUser()->getEmpresa()) {
            return $this->redirectToRoute("panel_inicio");
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($reserva);
        $em->flush();
        return $this->redirectToRoute('panel_inicio');
    }

    /**
     * Creates a form to delete a Reserva entity.
     *
     * @param Reserva $reserva The Reserva entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Reserva $reserva)
    {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('reserva_delete', array('id' => $reserva->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

    /**
     *
     * @Route("/{id}/pdf/presupuesto", name="reserva_pdf_presupuesto")
     * @Method("GET")
     */
    public function pdfPresupuestoAction(Reserva $reserva)
    {
        if (!$this->isGranted("ROLE_RG_GLOBAL") && $reserva->getEmpresa() != $this->getUser()->getEmpresa() && $reserva->getComercial() != $this->getUser()->getEmpresa()) {
            return $this->redirectToRoute("panel_inicio");
        }

        $html = $this->renderView('AppBundle:Reserva:PDF-presupuesto.html.twig', array(
            'reserva' => $reserva
        ));
//        return new Response($html);

        return new Response(
                $this->get('knp_snappy.pdf')->getOutputFromHtml($html), 200, array(
            'Content-Type' => 'application/pdf',
//            'page-width'  => 1000,
//                'Content-Disposition'   => 'attachment; filename="RGSpain_Presupuesto_P'.$reserva->getId().'.pdf"'
                )
        );
    }

    /**
     *
     * @Route("/{id}/pdf/proforma", name="reserva_pdf_proforma")
     * @Method("GET")
     */
    public function pdfProformaAction(Reserva $reserva)
    {
        if (!$this->isGranted("ROLE_RG_GLOBAL") && $reserva->getEmpresa() != $this->getUser()->getEmpresa() && $reserva->getComercial() != $this->getUser()->getEmpresa()) {
            return $this->redirectToRoute("panel_inicio");
        }

        $html = $this->renderView('AppBundle:Reserva:PDF-proforma.html.twig', array(
            'reserva' => $reserva
        ));

        return new Response(
                $this->get('knp_snappy.pdf')->getOutputFromHtml($html), 200, array(
            'Content-Type' => 'application/pdf',
//                'Content-Disposition'   => 'attachment; filename="RGSpain_Presupuesto_P'.$reserva->getId().'.pdf"'
                )
        );
    }

    /**
     * @Route("/{id}/pdf/albaran", name="reserva_pdf_albaran")
     * @Method("GET")
     */
    public function pdfAlbaranAction(Reserva $reserva)
    {
        if (!$this->isGranted("ROLE_RG_GLOBAL") && $reserva->getEmpresa() != $this->getUser()->getEmpresa() && $reserva->getComercial() != $this->getUser()->getEmpresa()) {
            return $this->redirectToRoute("panel_inicio");
        }

        $html = $this->renderView('AppBundle:Reserva:PDF-albaran.html.twig', array(
            'reserva' => $reserva
        ));
//        return new Response($html);

        return new Response(
                $this->get('knp_snappy.pdf')->getOutputFromHtml($html), 200, array(
            'Content-Type' => 'application/pdf',
//                'Content-Disposition'   => 'attachment; filename="RGSpain_Albaran_A'.$reserva->getId().'.pdf"'
                )
        );
    }

    /**
     * @Route("/{id}/pdf/recibo", name="reserva_pdf_recibo")
     * @Method("GET")
     */
    public function pdfReciboAction(Reserva $reserva)
    {
        if (!$this->isGranted("ROLE_RG_GLOBAL") && $reserva->getEmpresa() != $this->getUser()->getEmpresa() && $reserva->getComercial() != $this->getUser()->getEmpresa()) {
            return $this->redirectToRoute("panel_inicio");
        }

        $html = $this->renderView('AppBundle:Reserva:PDF-recibo.html.twig', array(
            'reserva' => $reserva
        ));


        return new Response(
                $this->get('knp_snappy.pdf')->getOutputFromHtml($html, array(
                        //'orientation'=>'Landscape',
                        //'page-size'                    => "A5",
                )), 200, array(
            'Content-Type' => 'application/pdf',
//                'Content-Disposition'   => 'attachment; filename="RGSpain_Recibo_R'.$reserva->getId().'.pdf"'
                )
        );
    }

    /**
     * Finds and displays a Reserva entity.
     *
     * @Route("/{id}/pdf/factura", name="reserva_pdf_factura")
     * @Method("GET")
     */
    public function pdfFacturaAction(Reserva $reserva)
    {
        if (!$this->isGranted("ROLE_RG_GLOBAL") && $reserva->getEmpresa() != $this->getUser()->getEmpresa() && $reserva->getComercial() != $this->getUser()->getEmpresa()) {
            return $this->redirectToRoute("panel_inicio");
        }

        $html = $this->renderView('AppBundle:Reserva:PDF-factura.html.twig', array(
            'reserva' => $reserva
        ));

        return new Response(
                $this->get('knp_snappy.pdf')->getOutputFromHtml($html), 200, array(
            'Content-Type' => 'application/pdf',
//                'Content-Disposition'   => 'attachment; filename="RGSpain_Factura_F'.$reserva->getId().'.pdf"'
                )
        );
    }

    /**
     * Agrega un histórico.
     *
     * @Route("/{id}/addhistorico", name="historico_add")
     * @Method("POST")
     */
    public function historicoAddAction(Request $request, Reserva $reserva)
    {
        $form = $this->createDeleteForm($reserva);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($reserva);
            $em->flush();
        }

        return $this->redirectToRoute('reserva_index');
    }

}
