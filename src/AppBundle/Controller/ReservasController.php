<?php

namespace AppBundle\Controller;

use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/reservas")
 */
class ReservasController extends Controller
{

    protected $em;

    public function __construct(EntityManager $em = null)
    {
        if (!is_null($em)) {
            $this->em = $em;
        }
    }

    protected function em()
    {
        if (is_null($this->em)) {
            $this->em = $this->getDoctrine()->getManager();
        }
        return $this->em;
    }

    /**
     * Todas las reservas del sistema
     * @Route("/", name="reservas")
     */
    public function indexAction(Request $request)
    {

        $queryBuilder = $this->getDoctrine()->getManager()->createQueryBuilder();
        $query = $queryBuilder
                ->select('r')
                ->from('AppBundle:Reserva', 'r');

        $filtro = $this->applyFilters($request, $query);

        $reservas = $query->getQuery()->getResult();
        return $this->render('AppBundle:Default:listado_reservas.html.twig', array(
                    'reservas' => $reservas,
                    'titulotabla' => 'Todas las reservas del sistema',
                    'title' => 'Reservas-Listado',
                    'filtro' => $filtro,
        ));
    }

//    public function listarAction($estado = 'all', $max = 5)
//    {
//        $emReservas = $this->getDoctrine()->getRepository('AppBundle:Reserva');
//        if (!$this->isGranted('ROLE_RG_GLOBAL')) {
//            if ($estado == 'all') {
//                $queryBuilder = $this->em()->createQueryBuilder();
//                $query = $queryBuilder
//                        ->select('r')
//                        ->from('AppBundle:Reserva', 'r')
//                        ->leftJoin("r.comercial", "c")
//                        ->andWhere('c.empresa = :empresa OR r.empresa = :empresa')
//                        ->setParameter('empresa', $this->getUser()->getEmpresa());
//                $reservas = $query->getQuery()->getResult();
//            } else {
//                $reservas = $emReservas->listarEstado($estado, null, $this->getUser());
//            }
//        } else {
//            if ($estado == 'all') {
//                $reservas = $emReservas->findAll();
//            } else {
//                $reservas = $emReservas->listarEstado($estado);
//            }
//        }
//
//        if ($max !== null) {
////            $reservas->limit($max);
//            $reservas = array_slice($reservas, 0, $max);
//        }
//        return $this->render('AppBundle:Modulos:Reservas.html.twig', array(
//                    'reservas' => $reservas,
//        ));
//    }

    /**
     * Reservas con estado "presupuesto"
     * @Route("/pendientes", name="reservas_pendientes")
     */
    public function listadoPendientesAction(Request $request)
    {
        $queryBuilder = $this->getDoctrine()->getManager()->createQueryBuilder();
        $query = $queryBuilder
                ->select('r')
                ->from('AppBundle:Reserva', 'r');

        $filtro = $this->applyFilters($request, $query, false, $this->em()->getRepository(\AppBundle\Entity\Estado\Reserva::class)->findOneByNombre("PRESUPUESTO"));

        $reservas = $query->getQuery()->getResult();

        return $this->render('AppBundle:Default:listado_reservas.html.twig', array(
                    'reservas' => $reservas,
                    'titulotabla' => 'Presupuestos pendientes de aceptación',
                    'title' => 'Reservas Pendientes',
                    'filtro' => $filtro
        ));
    }

    /**
     * Página de reservas activas
     * @Route("/activas", name="reservas_activas")
     */
    public function listadoActivasAction(Request $request)
    {
        $queryBuilder = $this->em()->createQueryBuilder();
        $query = $queryBuilder
                ->select('r')
                ->from('AppBundle:Reserva', 'r')
                ->where('r.estado IN (SELECT re.id FROM AppBundle\Entity\Estado\Reserva re WHERE re.activa=1 )');

        $filtros = $this->applyFilters($request, $query);
        $reservas = $query->getQuery()->getResult();

        return $this->render('AppBundle:Default:listado_reservas.html.twig', array(
                    'reservas' => $reservas,
                    'titulotabla' => 'Reservas activas',
                    'title' => 'Reservas Activas',
                    'filtro' => $filtros
        ));
    }

    /**
     * Página de reservas completas
     * @Route("/completas", name="reservas_completas")
     */
    public function listadoCompletasAction(Request $request)
    {
        $queryBuilder = $this->getDoctrine()->getManager()->createQueryBuilder();
        $query = $queryBuilder
                ->select('r')
                ->from('AppBundle:Reserva', 'r');

        $filtro = $this->applyFilters($request, $query, false, $this->em()->getRepository(\AppBundle\Entity\Estado\Reserva::class)->findOneByNombre("FINALIZADA"));
        $reservas = $query->getQuery()->getResult();


        return $this->render('AppBundle:Default:listado_reservas.html.twig', array(
                    'reservas' => $reservas,
                    'titulotabla' => 'Reservas completadas',
                    'title' => 'Reservas Completas',
                    'filtro' => $filtro
        ));
    }

    /**
     * Reservas con duración superior a 28 días
     * @Route("/largaduracion", name="reservas_largaduracion")
     */
    public function listadoLargaDuracionAction(Request $request)
    {

        $queryBuilder = $this->em()->createQueryBuilder();
        $query = $queryBuilder
                ->select('r')
                ->from('AppBundle:Reserva', 'r')
                ->where("r.fechaFin > DATE_ADD(r.fechaInicio, 28, 'DAY' )");

        $filtro = $this->applyFilters($request, $query, true);

        $reservas = $query->getQuery()->getResult();

        return $this->render('AppBundle:Default:listado_reservas.html.twig', array(
                    'reservas' => $reservas,
                    'titulotabla' => 'Reservas de larga duración',
                    'title' => 'Reservas larga duración',
                    'filtro' => $filtro
        ));
    }

    /**
     * Página de reservas activas
     * @Route("/canceladas", name="reservas_canceladas")
     */
    public function listadoCanceladasAction(Request $request)
    {
        $queryBuilder = $this->getDoctrine()->getManager()->createQueryBuilder();
        $query = $queryBuilder
                ->select('r')
                ->from('AppBundle:Reserva', 'r');

        $filtro = $this->applyFilters($request, $query, false, $this->em()->getRepository(\AppBundle\Entity\Estado\Reserva::class)->findOneByNombre("CANCELADO"));
        $reservas = $query->getQuery()->getResult();

        return $this->render('AppBundle:Default:listado_reservas.html.twig', array(
                    'reservas' => $reservas,
                    'titulotabla' => 'Reservas canceladas',
                    'title' => 'Reservas Canceladas',
                    'filtro' => $filtro,
        ));
    }

    public function applyFilters(Request $request, \Doctrine\ORM\QueryBuilder &$query, $lduracion = false, \AppBundle\Entity\Estado\Reserva $estado = null)
    {
        $inicio = $request->query->get('inicio');
        $fin = $request->query->get('fin');
        $estado = (is_null($estado)) ? $request->query->get('estado') : $estado;
        $modelo = $request->query->get('modelo');
        $cliente = $request->query->get('cliente');
        $lduracion = ($lduracion) ? 'on' : $request->query->get('lduracion');

        if (!is_null($inicio) && !empty($inicio)) {
            $query->andWhere("r.fechaInicio >= :fini ");
            $query->setParameter("fini", $inicio);
        }
        if (!is_null($fin) && !empty($fin)) {
            $query->andWhere("r.fechaFin <= :ffin ");
            $query->setParameter("ffin", $fin);
        }
        if (!is_null($modelo) && !empty($modelo)) {
            $query->andWhere("r.modelo = :modelo ");
            $query->setParameter("modelo", $modelo);
        }
        if ($lduracion != 'on') {
            $query->andWhere("r.fechaFin <= DATE_ADD(r.fechaInicio, 28, 'DAY' )");
        }
        if (!is_null($cliente) && !empty($cliente)) {
            $query->andWhere("r.empresa = :empresa ");
            $query->setParameter("empresa", $cliente);
        }

        if (!is_null($estado) && !empty($estado)) {
            if (!$estado instanceof \AppBundle\Entity\Estado\Reserva) {
                $estado = $this->em()->getRepository(\AppBundle\Entity\Estado\Reserva::class)->find($estado);
            }
            $query->andWhere("r.estado = :estado ");
            $query->setParameter("estado", $estado);
        }

        if (!$this->isGranted('ROLE_RG_GLOBAL')) {
            $query->leftJoin("r.comercial", "c")
                    ->andWhere('c.empresa = :empresa OR r.empresa = :empresa')
                    ->setParameter('empresa', $this->getUser()->getEmpresa());
        }


        if (!$this->isGranted('ROLE_RG_GLOBAL')) {
            $empresas = [$this->getUser()->getEmpresa()];
        } else {
            $empresas = $this->em()->getRepository(\AppBundle\Entity\Empresa::class)->findAll();
        }
        $estados = $this->em()->getRepository(\AppBundle\Entity\Estado\Reserva::class)->findAll();

        return array(
            'opciones' => [
                'estados' => $estados,
                'clientes' => $empresas
            ],
            'valores' => array(
                'inicio' => $inicio,
                'fin' => $fin,
                'estado' => $estado,
                'modelo' => $modelo,
                'cliente' => $cliente,
                'lduracion' => $lduracion
            )
        );
    }

}
