<?php

namespace AppBundle\Controller;

use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\Response;

class FiltrosController extends Controller
{
    protected $em=null;

    public function __construct(EntityManager $em=null)
    {
        if(!is_null($em)){
            $this->em = $em;
        }
    }
    
    protected function em(){
        if(is_null($this->em)){
            $this->em = $this->getDoctrine()->getManager();
        }
        return $this->em;
    }
    
    public function estados($ambito){
        $queryBuilder = $this->em()->createQueryBuilder();
        $query = $queryBuilder
                ->select('e')
                ->from('AppBundle:Estado', 'e')
                ->where('e.ambito = \''.$ambito.'\'');
        return $query->getQuery()->getResult();
        
        //return $this->getDoctrine()->getRepository('AppBundle:Estado')->findByAmbito('reserva');
    }
    
    public function clientes(){
        $queryBuilder = $this->em()->createQueryBuilder();
        $query = $queryBuilder
                ->select('e')
                ->from('AppBundle:Empresa', 'e');
        return $query->getQuery()->getResult();
        
        //return $this->getDoctrine()->getRepository('AppBundle:Empresa')->findAll();
    }

    
}
