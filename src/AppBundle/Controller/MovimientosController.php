<?php

namespace AppBundle\Controller;

use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Movimiento controller.
 *
 * @Route("/panel/movimientos")
 */
class MovimientosController extends Controller
{

    protected $em = null;

    public function __construct(EntityManager $em = null)
    {
        if (!is_null($em)) {
            $this->em = $em;
        }
    }

    protected function em()
    {
        if (is_null($this->em)) {
            $this->em = $this->getDoctrine()->getManager();
        }
        return $this->em;
    }

    /**
     * @Route("/")
     */
    public function listarAction($estado = 'all', $max = 5)
    {
        $emReservas = $this->getDoctrine()->getRepository('AppBundle:Reserva');
        if ($estado == 'all') {
            $reservas = $emReservas->findAll();
        } else {
            $reservas = $emReservas->findByEstado(3);
        }

        if ($max !== null) {
//            $reservas->limit($max);
            $reservas = array_slice($reservas, 0, $max);
        }
        return $this->render('AppBundle:Modulos:Reservas.html.twig', array(
                    'reservas' => $reservas,
        ));
    }

    /**
     * @Route("/detalle/{id}", requirements={"id" = "\d+"})
     */
    public function detalleAction($id)
    {
        $movimiento = $this->getDoctrine()->getRepository('AppBundle:Movimiento')
                ->find($id);
        $estados = $this->getDoctrine()->getRepository('AppBundle:Estado')
                ->findByAmbito('movimiento');

        return $this->render('AppBundle:Default:movimiento_detalle.html.twig', array(
                    'movimiento' => $movimiento,
                    'estados' => $estados
        ));
    }

    /**
     * @Route("/editAjax/{id}", requirements={"id" = "\d+"})
     */
    public function editAjaxAction($id)
    {
        $movimiento = $this->getDoctrine()->getRepository('AppBundle:Movimiento')
                ->find($id);
        $estados = $this->getDoctrine()->getRepository('AppBundle:Estado')
                ->findByAmbito('movimiento');

        return $this->render('AppBundle:Default:movimiento_detalle.html.twig', array(
                    'movimiento' => $movimiento,
                    'estados' => $estados
        ));
    }

    /**
     * Muestra las reservas pendientes de recogida y entrega
     * @Route("/pendientes", name="movimientos_pendientes")
     */
    public function pendientesAction()
    {
        $em = $this->getDoctrine()->getManager();
        if ($this->isGranted('ROLE_RG_GLOBAL')) {
            $entregas = $em->getRepository(\AppBundle\Entity\Movimiento::class)->findByTipo("entrega", "activo");
            $recogidas = $em->getRepository(\AppBundle\Entity\Movimiento::class)->findByTipo("recogida", "activo");
        } else {
            $entregas = $em->getRepository(\AppBundle\Entity\Movimiento::class)->findByTipo("entrega", "nofin", $this->getUser());
            $recogidas = $em->getRepository(\AppBundle\Entity\Movimiento::class)->findByTipo("recogida", "nofin", $this->getUser());
        }

        return $this->render('AppBundle:Default:movimientos.html.twig', array(
                    'entregas' => $entregas,
                    'recogidas' => $recogidas,
                    'title' => 'Listado-Movimientos',
        ));
    }

    /**
     * Muestra las reservas pendientes de recogida y entrega
     * @Route("/pendientes/pdf", name="movimientos_pendientes_pdf")
     */
    public function pendientesPdfAction()
    {
        $em = $this->getDoctrine()->getManager();
        if ($this->isGranted('ROLE_RG_GLOBAL')) {
            $entregas = $em->getRepository(\AppBundle\Entity\Movimiento::class)->findByTipo("entrega");
            $recogidas = $em->getRepository(\AppBundle\Entity\Movimiento::class)->findByTipo("recogida");
        } else {
            $entregas = $em->getRepository(\AppBundle\Entity\Movimiento::class)->findByTipo("entrega", null, $this->getUser());
            $recogidas = $em->getRepository(\AppBundle\Entity\Movimiento::class)->findByTipo("recogida", null, $this->getUser());
        }

        $html = $this->renderView('AppBundle:PDF:movimientos.html.twig', array(
            'entregas' => $entregas,
            'recogidas' => $recogidas,
            'title' => 'Listado-Movimientos',
        ));

        $snappy = $this->get('knp_snappy.pdf');
//            $snappy->setOption('header-html', $this->container->get('templating.helper.assets')->getUrl('bundles/core/pdfHtml/header.html'));
//            $snappy->setOption('footer-html', $this->container->get('templating.helper.assets')->getUrl('bundles/core/pdfHtml/footer.html'));
        $snappy->setOption('orientation', 'Landscape');
        $snappy->setOption('footer-right', 'Página [page] de [topage] - ' . date('\ d.m.Y\ H:i'));
        $snappy->setOption('footer-left', 'RGSpain');
        return new Response(
                $snappy->getOutputFromHtml($html), 200, array(
            'Content-Type' => 'application/pdf',
                // 'Content-Disposition'   => 'attachment; filename="RGSpain_Presupuesto_R'.$reserva->getId().'.pdf"'
                )
        );
    }

    /**
     * Renderiza los movimientos de entrega y recogida para módulos que son mostrados dentro de otros controladores
     */
    public function pendientesModuloAction($tipo, $max = null)
    {
        if ($tipo == "entrega") {
            return $this->render('AppBundle:Modulos:ReservaEntregas.html.twig', array(
                        'reservas' => $this->buscarEntregas($max),
                        'tipo' => $tipo
            ));
        }

        if ($tipo == "recogida") {
            $reservas = $this->buscarRecogidas();
            return $this->render('AppBundle:Modulos:ReservaRecogidas.html.twig', array(
                        'reservas' => $this->buscarRecogidas($max),
                        'tipo' => $tipo
            ));
        }
    }

    public function buscarAction($tipo = "entrega", $fecha = null, $estado_reserva = "Aceptado", $estado_movimiento = "En espera", $max = null)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQueryBuilder()
                ->select('r')
                ->from('AppBundle:Reserva', 'r')
                ->where('r.estado = (SELECT e1.id FROM AppBundle:Estado e1 WHERE e1.nombre = :estado_reserva)')
                //->andWhere('r.'.$tipo.' IS NOT NULL')
//                ->andWhere('r.entrega')
//                ->andWhere('r.'.$tipo.' IN (SELECT m1.id FROM AppBundle:Movimiento m1 WHERE m1.fecha >= :hoy)')
//                ->andWhere('m.estado = (SELECT e2.id FROM AppBundle:Estado e2 WHERE e2.nombre = :estado_movimiento)')
//                ->andWhere('m.fecha >= :hoy')
//                ->groupBy('m.id')
                ->orderBy('r.fechaInicio', 'ASC');

//                if($fecha !== null){
//                    $query->setParameter('hoy', date("Y-m-d",strtotime($fecha)));
//                }else{
//                    $query->setParameter('hoy', date("Y-m-d"));
//                }
        $query->setParameter('estado_reserva', $estado_reserva);
//                ->setParameter('estado_movimiento', $estado_movimiento)
        $reservas = $query->getQuery()->getResult();

        if ($max !== null) {
            return array_slice($reservas, 0, $max);
        }
        return $reservas;
    }

    public function buscar2Action($tipo = "entrega", $fecha = null, $estado_reserva = "Aceptado", $estado_movimiento = "En espera")
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQueryBuilder()
                ->select('m, sum(r.cantidadReceptores) as total')
                ->from('AppBundle:Movimiento', 'm')
                ->from('AppBundle:Reserva', 'r')
                ->where('r.estado = (SELECT e1.id FROM AppBundle:Estado e1 WHERE e1.nombre = :estado_reserva)')
                ->andWhere('r.' . $tipo . ' IN (SELECT m1.id FROM AppBundle:Movimiento m1 WHERE m1.fecha >= :hoy)')
                ->andWhere('m.estado = (SELECT e2.id FROM AppBundle:Estado e2 WHERE e2.nombre = :estado_movimiento)')
                ->andWhere('m.fecha >= :hoy')
//                ->groupBy('m.id')
                ->orderBy('m.fecha', 'ASC');

        if ($fecha !== null) {
            $query->setParameter('hoy', date("Y-m-d", strtotime($fecha)));
        } else {
            $query->setParameter('hoy', date("Y-m-d"));
        }
        $query
                ->setParameter('estado_reserva', $estado_reserva)
                ->setParameter('estado_movimiento', $estado_movimiento)
                ->getQuery();
        return $query->getQuery()->getResult();
    }

}
