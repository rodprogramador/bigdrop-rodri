<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        if ($this->getUser() instanceof \AppBundle\Entity\Usuario) {
            return $this->redirectToRoute('panel_inicio');
        } else {
            return $this->redirectToRoute('login');
        }
    }

    /**
     * @Route("/login", name="login")
     */
    public function loginAction(Request $request)
    {
        if ($this->getUser() instanceof \AppBundle\Entity\Usuario) {
            return $this->redirectToRoute('panel_inicio');
        }
        $form = $this->createForm(\AppBundle\Form\Type\LoginType::class, new \AppBundle\Entity\Usuario());

        $exception = $this->get('security.authentication_utils')
                ->getLastAuthenticationError();

        return $this->render('AppBundle:Default:login.html.twig', array(
                    'form' => $form->createView(),
                    'error' => $exception ? $exception->getMessage() : NULL
        ));
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logOutAction()
    {
        return $this->redirectToRoute('login');
    }

    public function alquileresChartModuloAction()
    {
        $reservas = $this->get('rgspain.reservas')->listarActivas();
        $return = array();
        foreach ($reservas as $reserva) {
            for ($f = $reserva->getFechaInicio(); $f <= $reserva->getFechaFin(); $f->add(new \DateInterval('P1D'))) {
                $return[$f->format("d-m-Y")][$reserva->getModelo()] = + $reserva->getCantidadReceptores();
            }
        }
        return $this->render('AppBundle:Modulos:AlquileresChart.html.twig', array(
                    'datos' => $return,
        ));
    }

}
