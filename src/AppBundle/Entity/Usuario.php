<?php

/*
 * The MIT License
 *
 * Copyright 2017 Víctor García <vgpastor@ingenierosweb.co>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace AppBundle\Entity;

use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Description of Usuario
 *
 * @author Víctor García <vgpastor@ingenierosweb.co>
 */

/**
 *
 * @ORM\Table(name="fos_user_user")
 * @ORM\Entity
 */
class Usuario implements AdvancedUserInterface
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    protected $email;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    protected $username;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255)
     */
    protected $password;

    /**
     * Many Features have One Product.
     * @ORM\ManyToOne(targetEntity="Empresa", inversedBy="usuarios")
     * @ORM\JoinColumn(name="empresa_id", referencedColumnName="id")
     */
    private $empresa;

    /**
     * @var bool
     *
     * @ORM\Column(name="locked", type="boolean")
     */
    protected $locked = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="roles", type="array")
     */
    protected $roles;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function isAccountNonExpired()
    {
        return true;
    }

    public function isAccountNonLocked()
    {
        return !$this->locked;
    }

    public function isCredentialsNonExpired()
    {
        return true;
    }

    public function isEnabled()
    {
        return true;
    }

    public function eraseCredentials()
    {

    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getRoles()
    {
        return $this->roles;
    }

    public function getSalt()
    {
        return md5($this->email);
    }

    public function getUsername()
    {
        return $this->email;
    }

    public function setPassword($pwdplain = null)
    {
        if (!is_null($pwdplain) && strlen($pwdplain) >= 3) {
            $this->password = hash_hmac('SHA512', $pwdplain, $this->getSalt());
        }
        return $this;
    }

    public function verifyPassword($password)
    {
        return true;
        if (hash_hmac('SHA512', $password, $this->getSalt()) == $this->getPassword()) {
            return true;
        }
        return false;
    }

    public function __toString()
    {
        return $this->getEmail();
    }

    public function avatar($size = 512)
    {
        return "http://www.gravatar.com/avatar/" . md5($this->getEmail()) . ".jpg";
    }

    function getEmpresa()
    {
        return $this->empresa;
    }

    function setEmpresa($empresa)
    {
        $this->empresa = $empresa;
    }

    function getLocked()
    {
        return $this->locked;
    }

    function setLocked($locked)
    {
        $this->locked = $locked;
    }

    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    public function setRoles($roles)
    {
        $this->roles = $roles;
        return $this;
    }

    public function addRole($role)
    {
        $this->roles[] = $role;
        return $this;
    }

}
