<?php

namespace AppBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use AppBundle\Model\Objeto;

/**
 *
 * @ORM\Table(name="reserva")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ReservaRepository")
 * @ORM\HasLifecycleCallbacks()
 *
 */
class Reserva
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fechaAlta;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fechaUpdate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaInicio", type="datetime")
     */
    protected $fechaInicio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaFin", type="datetime")
     */
    protected $fechaFin;

    /**
     *
     * @var String
     *
     * @ORM\Column(name="circuito", type="string")
     *
     */
    protected $circuito;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Empresa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="empresa", referencedColumnName="id")
     * })
     */
    protected $empresa;

    /**
     * @var Integer
     *
     * @ORM\Column(name="cantidad_receptores", type="integer")
     */
    protected $cantidadReceptores;

    /**
     * @var Integer
     *
     * @ORM\Column(name="cantidad_emisores", type="integer")
     */
    protected $cantidadEmisores;

    /**
     * @var Integer
     *
     * @ORM\Column(name="cantidad_reserva", type="integer")
     */
    protected $cantidadReserva = 2;

    /**
     * @var string
     *
     * @ORM\Column(name="modelo", type="string")
     */
    protected $modelo = "SAU";

    /**
     * @var Movimiento
     *
     * @ORM\ManyToOne(targetEntity="Movimiento", inversedBy="reserva", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="entrega", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $entrega;

    /**
     * @var Movimiento
     *
     * @ORM\ManyToOne(targetEntity="Movimiento", inversedBy="reserva",cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="recogida", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $recogida;

    /**
     * @var Estado\Reserva
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Estado\Reserva")
     * @ORM\JoinColumn(name="estado", referencedColumnName="id")
     */
    protected $estado;

    /**
     * @var float
     *
     * @ORM\Column(name="precio", type="float")
     */
    protected $precio = 1.8;

    /**
     * @var String
     *
     * @ORM\Column(name="precio_tipo", type="string")
     */
    protected $precioTipo = "simple";

    /**
     * @var float
     *
     * @ORM\Column(name="precio_impuestos", type="float")
     */
    protected $precioImpuestos = 21;

    /**
     * @var float
     *
     * @ORM\Column(name="precio_transporte", type="float")
     */
    protected $precioTransporte = 20;

    /**
     *
     * @ORM\Column(name="peticion", type="text",nullable=true)
     */
    protected $peticion;

    /**
     *
     * @ORM\Column(name="notas", type="text", nullable=true)
     */
    protected $notas;

    /**
     * @ORM\ManyToOne(targetEntity="Iweb\FactuBundle\Entity\Factura", inversedBy="pedido")
     * @ORM\JoinColumn(name="factura_id", referencedColumnName="id")
     */
    protected $factura;

    /**
     * @var Usuario
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Usuario")
     * @ORM\JoinColumn(name="comercial", referencedColumnName="id")
     */
    protected $comercial;

    public function __construct()
    {
//        $this->estado = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     *
     * @return \Date
     */
    public function getFechaInicio()
    {
        return $this->fechaInicio;
    }

    /**
     *
     * @return \Date
     */
    public function getFechaFin()
    {
        return $this->fechaFin;
    }

    /**
     *
     * @return Integer
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     *
     * @return Circuito
     */
    public function getCircuito()
    {
        return $this->circuito;
    }

//    public function addEntrega($movimiento)
//    {
//        $this->entrgega[] = $movimiento;
//        return $this;
//    }
//    public function addRecogida($movimiento)
//    {
//        $this->recogida[] = $movimiento;
//        return $this;
//    }
    /**
     *
     * @return Movimiento
     */
    public function getEntrega()
    {
        return $this->entrega;
    }

    /**
     *
     * @return Movimiento
     */
    public function getRecogida()
    {
        return $this->recogida;
    }

    public function __toString()
    {
        return "Reserva " . $this->getId();
    }

    public function setFechaInicio(\DateTime $fechaInicio)
    {
        $this->fechaInicio = $fechaInicio;
        return $this;
    }

    public function setFechaFin(\DateTime $fechaFin)
    {
        $this->fechaFin = $fechaFin;
        return $this;
    }

    public function setCircuito($circuito)
    {
        $this->circuito = $circuito;
        return $this;
    }

    public function setEmpresa(Empresa $empresa)
    {
        $this->empresa = $empresa;
        if ($empresa->getPrecioAlquiler() !== null) {
            $this->precio = $empresa->getPrecioAlquiler();
        }
        if ($empresa->getPrecioTipo() !== null) {
            $this->precioTipo = $empresa->getPrecioTipo();
        }
        if ($empresa->getPrecioTransporte() !== null) {
            $this->precioTransporte = $empresa->getPrecioTransporte();
        }
        return $this;
    }

    public function setCantidadReceptores($cantidadReceptores)
    {
        $this->cantidadReceptores = $cantidadReceptores;
        return $this;
    }

    public function setCantidadEmisores($cantidadEmisores)
    {
        $this->cantidadEmisores = $cantidadEmisores;
        return $this;
    }

    public function setCantidadReserva($cantidadReserva)
    {
        $this->cantidadReserva = $cantidadReserva;
        return $this;
    }

    public function setModelo($modelo)
    {
        $this->modelo = $modelo;
        return $this;
    }

    public function setEntrega(Movimiento $entrega)
    {
        $entrega->setReserva($this);
        $this->entrega = $entrega;
        return $this;
    }

    public function setRecogida(Movimiento $recogida)
    {
        $recogida->setReserva($this);
        $this->recogida = $recogida;
        return $this;
    }

    public function setPrecio($precio)
    {
        $this->precio = $precio;
        return $this;
    }

    public function setPrecioTipo($precioTipo)
    {
        $this->precioTipo = $precioTipo;
        return $this;
    }

    public function setPrecioImpuestos($precioImpuestos)
    {
        $this->precioImpuestos = $precioImpuestos;
        return $this;
    }

    public function setPrecioTransporte($precioTransporte)
    {
        $this->precioTransporte = $precioTransporte;
        return $this;
    }

    public function getEstado()
    {
        return $this->estado;
    }

    public function setEstado(Estado\Reserva $estado)
    {
        $this->estado = $estado;
        return $this;
    }

    public function getModelo()
    {
        return $this->modelo;
    }

    public function getPrecio()
    {
        return $this->precio;
    }

    public function getEmpresa()
    {
        return $this->empresa;
    }

    public function getCantidadReceptores()
    {
        return $this->cantidadReceptores;
    }

    public function getCantidadEmisores()
    {
        return $this->cantidadEmisores;
    }

    public function getCantidadReserva()
    {
        return $this->cantidadReserva;
    }

    public function getPrecioTipo()
    {
        return $this->precioTipo;
    }

    public function getPrecioImpuestos()
    {
        return $this->precioImpuestos;
    }

    public function getPrecioTransporte()
    {
        return $this->precioTransporte;
    }

    public function getEntregaFecha()
    {
        return $this->entregaFecha;
    }

    public function getEntregaDireccion()
    {
        return $this->entregaDireccion;
    }

    public function getEntregaEstado()
    {
        return $this->entregaEstado;
    }

    public function getRecogidaFecha()
    {
        return $this->recogidaFecha;
    }

    public function getRecogidaDias()
    {
        $now = new \DateTime('now');
        $d = $now->diff($this->getRecogidaFecha()); //->format("%r%d");
        if ($d->format("%H") > 0 && $d->format("%r") != "-") {
            $r = $d->format("%r%a") + 1;
        } else {
            $r = $d->format("%r%a");
        }
        return $r;
    }

    public function getRecogidaDireccion()
    {
        return $this->recogidaDireccion;
    }

    public function getRecogidaEstado()
    {
        return $this->recogidaEstado;
    }

    public function setEntregaEstado($entregaEstado)
    {
        $this->entregaEstado = $entregaEstado;
        return $this;
    }

    public function setRecogidaEstado($recogidaEstado)
    {
        $this->recogidaEstado = $recogidaEstado;
        return $this;
    }

    public function getPeticion()
    {
        return ($this->peticion);
    }

    public function getNotas()
    {
        return $this->notas;
    }

    public function setPeticion($peticion)
    {
        $this->peticion = $peticion;
    }

    public function setNotas($notas)
    {
        $this->notas = $notas;
    }

    public function getNumDias()
    {
        $interval = $this->fechaInicio->diff($this->fechaFin);
        return $interval->format('%a') + 1;
    }

    public function getTotalReceptores()
    {
        switch ($this->getPrecioTipo()) {
            case 'diario':
                //Precio por día para todos los aparatos
                $subtotal = ($this->getPrecio() * $this->getNumDias());
                break;
            case 'completo':
                //Precio final de la reserva
                $subtotal = $this->getPrecio();
                break;
            case 0:
            case 'simple':
            default:
                //Precio por aparato y dia
                $subtotal = ($this->getPrecio() * $this->getCantidadReceptores() * $this->getNumDias());
                break;
        }
        return $subtotal;
    }

    /**
     * Total sin transporte ni impuestos
     */
    public function getTotalBruto()
    {
        return $this->getTotalReceptores() + $this->getPrecioTransporte();
    }

    public function getTotal()
    {
        if ($this->getPrecioImpuestos() > 0) {
            return (($this->getTotalBruto() / 100) * $this->getPrecioImpuestos()) + $this->getTotalBruto();
        }
        return $this->getTotalBruto();
    }

    public function setFactura($factura)
    {
        $this->factura = $factura;
        return $this;
    }

    public function getFactura()
    {
        if (!isset($this->factura) || empty($this->factura) || $this->factura == false) {
            return null;
        }
        return $this->factura;
    }

    public function isFacturado()
    {
        if (!isset($this->factura) || empty($this->factura) || $this->factura == false) {
            return false;
        } else {
            return true;
        }
    }

    function getComercial()
    {
        return $this->comercial;
    }

    function setComercial(Usuario $comercial)
    {
        $this->comercial = $comercial;
    }

    /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
        $this->fechaAlta = new \DateTime();
        $this->fechaUpdate = $this->fechaAlta;
    }

    /**
     * @ORM\PreUpdate
     */
    public function preUpdate()
    {
        $this->fechaUpdate = new \DateTime();
    }

}
