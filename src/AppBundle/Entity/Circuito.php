<?php

namespace AppBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use AppBundle\Model\Objeto;
/**
 *
 * @ORM\Table(name="circuito")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CircuitoRepository")
 * 
*/
class Circuito extends Objeto
{
    
    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    protected $nombre;
    
    /**
     * @ORM\ManyToOne(targetEntity="Empresa")
     * @ORM\JoinColumn(name="empresa", referencedColumnName="id")
     */
    protected $empresa;
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId(){
        return $this->id;
    }
    
    /**
     * @return String
     */
    public function getNombre(){
        return $this->nombre;
    }
    
    public function getEmpresa(){
        return $this->empresa;
    }

}

