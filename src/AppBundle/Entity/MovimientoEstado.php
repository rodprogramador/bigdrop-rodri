<?php

namespace AppBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Sonata\TranslationBundle\Traits\Gedmo\PersonalTranslatable;
use \AppBundle\Model\Objeto;

/**
 *
 * @ORM\Table(name="movimiento_estado")
 * @ORM\Entity
 * 
*/
class MovimientoEstado extends Objeto
{
    /** 
     * @ORM\ManyToOne(targetEntity="Movimiento", inversedBy="estado") 
     * @ORM\JoinColumn(name="reserva_id", referencedColumnName="id", nullable=false) 
     */
    protected $movimiento;
    
    /** 
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Estado\Movimiento") 
     * @ORM\JoinColumn(name="estado_id", referencedColumnName="id", nullable=false) 
     */
    protected $estado;
    
    /**
     * @var \Date
     * 
     * @ORM\Column(name="fecha", type="date")
     */    
    protected $fecha;

}

