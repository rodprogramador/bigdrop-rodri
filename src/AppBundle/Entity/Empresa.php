<?php

namespace AppBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use \AppBundle\Model\Objeto;

/**
 *
 * @ORM\Table(name="empresa")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EmpresaRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Empresa
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="activa", type="boolean")
     */
    protected $activa;

    /**
     * @var string
     *
     * @ORM\Column(name="sociedad", type="string", length=255)
     
     */
    protected $sociedad;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    protected $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="cif", type="string", length=255, unique=true, nullable=true)
     */
    protected $cif;

    /**
     * @var string
     *
     * @ORM\Column(name="pcontacto", type="string", length=255)
     */
    protected $pContacto;

    /**
     * @var string
     *
     * @ORM\Column(name="tlfFijo", type="string", length=15, nullable=true)
     */
    protected $tlfFijo;

    /**
     * @var string
     *
     * @ORM\Column(name="tlfMovil", type="string", length=15, nullable=true)
     */
    protected $tlfMovil;

    /**
     * @var string
     *
     * @ORM\Column(name="emailContacto", type="string", length=50)
     */
    protected $emailContacto;

    /**
     * @var string
     *
     * @ORM\Column(name="emailFacturacion", type="string", length=50)
     */
    protected $emailFacturacion;

    /**
     * @var string
     *
     * @ORM\Column(name="web", type="string", length=50, nullable=true)
     */
    protected $web;

    /**
     * @var string
     *
     * @ORM\Column(name="notasFacturacion", type="string", length=255, nullable=true)
     */
    protected $notasFacturacion;

    /**
     * @ORM\ManyToOne(targetEntity="Direccion",cascade={"persist"})
     * @ORM\JoinColumn(name="dirFacturacion", referencedColumnName="id")
     */
    protected $dirFacturacion;

    /**
     * @ORM\ManyToOne(targetEntity="Direccion",cascade={"persist"})
     * @ORM\JoinColumn(name="dirEntrega", referencedColumnName="id")
     */
    protected $dirEntrega;

    /**
     * @ORM\OneToMany(targetEntity="Usuario",cascade={"persist"}, mappedBy="empresa")
     */
    protected $usuarios;

    /**
     * @var string
     *
     * @ORM\Column(type="float", nullable=true)
     */
    protected $precioAlquiler = 1.8;

    /**
     * @var string
     *
     * @ORM\Column(type="float", nullable=true)
     */
    protected $precioTransporte = 20;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    protected $precioTipo = "simple";

    /**
     * @ORM\Column(type="date",options={"default"="2017-01-01"})
     */
    protected $fechaCreacion;

    /**
     * @ORM\Column(type="date", options={"default"="2017-01-01"})
     */
    protected $fechaUpdate;

    public function __construct()
    {
        $this->usuarios = new ArrayCollection();
    }

    public function getSociedad()
    {
        return $this->sociedad;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function getCif()
    {
        return $this->cif;
    }

    public function getDirFacturacion()
    {
        return $this->dirFacturacion;
    }

    public function getPContacto()
    {
        return $this->pContacto;
    }

    public function getTlfFijo()
    {
        return $this->tlfFijo;
    }

    public function getTlfMovil()
    {
        return $this->tlfMovil;
    }

    public function getEmailContacto()
    {
        return $this->emailContacto;
    }

    public function getEmailFacturacion()
    {
        return $this->emailFacturacion;
    }

    public function getWeb()
    {
        return $this->web;
    }

    public function getNotasFacturacion()
    {
        return $this->notasFacturacion;
    }

    public function getActiva()
    {
        return $this->activa;
    }

    public function getId()
    {
        return $this->id;
    }

    public function __toString()
    {
        $nombre = $this->getNombre();
        if (strlen($nombre) > 3) {
            return $nombre;
        } else {
            return $this->getSociedad();
        }
    }

    public function setTlfFijo($tlfFijo)
    {
        $this->tlfFijo = str_replace(array(" ", "-", "_", "."), "", $tlfFijo);
        return $this;
    }

    public function setTlfMovil($tlfMovil)
    {
        $this->tlfMovil = str_replace(array(" ", "-", "_", "."), "", $tlfMovil);
        return $this;
    }

    function getUsuarios()
    {
        return $this->usuarios;
    }

    function addUsuario($usuario)
    {
        $this->usuarios[] = $usuario;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setActiva($activa)
    {
        $this->activa = $activa;
        return $this;
    }

    public function setSociedad($sociedad)
    {
        $this->sociedad = $sociedad;
        return $this;
    }

    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
        return $this;
    }

    public function setCif($cif)
    {
        $this->cif = $cif;
        return $this;
    }

    public function setPContacto($pContacto)
    {
        $this->pContacto = $pContacto;
        return $this;
    }

    public function setEmailContacto($emailContacto)
    {
        $this->emailContacto = $emailContacto;
        return $this;
    }

    public function setEmailFacturacion($emailFacturacion)
    {
        $this->emailFacturacion = $emailFacturacion;
        return $this;
    }

    public function setWeb($web)
    {
        $this->web = $web;
        return $this;
    }

    public function setNotasFacturacion($notasFacturacion)
    {
        $this->notasFacturacion = $notasFacturacion;
        return $this;
    }

    public function setDirFacturacion($dirFacturacion)
    {
        $this->dirFacturacion = $dirFacturacion;
        return $this;
    }

    /**
     * @return \AppBundle\Entity\Direccion
     */
    public function getDirEntrega()
    {
        return $this->dirEntrega;
    }

    public function setDirEntrega($dirEntrega)
    {
        $this->dirEntrega = $dirEntrega;
        return $this;
    }

    public function getPrecioAlquiler()
    {
        return $this->precioAlquiler;
    }

    public function getPrecioTransporte()
    {
        return $this->precioTransporte;
    }

    public function getPrecioTipo()
    {
        return $this->precioTipo;
    }

    public function setPrecioAlquiler($precioAlquiler)
    {
        $this->precioAlquiler = $precioAlquiler;
        return $this;
    }

    public function setPrecioTransporte($precioTransporte)
    {
        $this->precioTransporte = $precioTransporte;
        return $this;
    }

    public function setPrecioTipo($precioTipo)
    {
        $this->precioTipo = $precioTipo;
        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
        if (is_null($this->emailFacturacion)) {
            $this->emailFacturacion = $this->emailContacto;
        }
        $this->fechaCreacion = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     * @ORM\PrePersist
     */
    public function preUpdate()
    {
        $this->fechaUpdate = new \DateTime();
    }

}
