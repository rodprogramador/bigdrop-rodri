<?php

namespace AppBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 *
 * @ORM\Table(name="movimiento")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MovimientoRepository")
 * 
*/
class Movimiento
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\Estado\Movimiento")
     * @ORM\JoinColumn(name="estado", referencedColumnName="id")
     */
    protected $estado;
    
    /**
     * @var \Date
     * 
     * @ORM\Column(name="fecha", type="date")
     *  
     */
    protected $fecha;
    
    /**
     * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\Direccion")
     * @ORM\JoinColumn(name="direccion", referencedColumnName="id")
     */
    protected $direccion;
    
    /**
     * @ORM\Column(type="string")
     */
    protected $tipo;
    
    /**
     * @ORM\ManyToOne(targetEntity="Reserva", cascade={"remove"})
     * @ORM\JoinColumn(name="reserva", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $reserva;
        
    /**
     * @return Direccion
     */
    public function getDireccion(){
        return $this->direccion;
    }
    
    public function setFecha(\DateTime $fecha=null) {
        $this->fecha = $fecha;
        return $this;
    }

    public function setDireccion($direccion) {
        if($direccion instanceof Direccion){
            $this->direccion = (string)$direccion;
        }
        $this->direccion = $direccion;
        return $this;
    }

        /**
     * @return \DateTime
     */
    public function getFecha(){
        return $this->fecha;
    }
    
    public function __toString() {
        return "MOV ".$this->fecha->format("d-m-Y")." ".$this->direccion;
    }
    
    public function getId() {
        return $this->id;
    }
    
    /**
     * @return Estado\Movimiento
     */
    public function getEstado(){
        return $this->estado;
    }
    
    public function setEstado(Estado\Movimiento $estado){
        $this->estado = $estado;
        return $this;
    }
    
    /**
     * @return \AppBundle\Entity\Reserva
     */
    public function getReserva() {
        return $this->reserva;
    }

    public function setReserva(\AppBundle\Entity\Reserva $reserva = null) {
        if(!is_null($reserva)){
            $this->reserva = $reserva;
        }
        return $this;
    }

    public function getTipo() {
        return $this->tipo;
    }

    public function setTipo($tipo) {
        $this->tipo = $tipo;
        return $this;
    }

    public function getRestoDias(){
        $now = new \DateTime('now');
        $d = $now->diff($this->getFecha());
        if($d->format("%H") > 0 && $d->format("%r") != "-"){
            $r = $d->format("%r%a")+1;
        }else{
            $r = $d->format("%r%a");
        }
        return $r;
    }

    
}

