<?php

namespace AppBundle\Entity\Estado;

use Gedmo\Mapping\Annotation as Gedmo;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Sonata\TranslationBundle\Traits\Gedmo\PersonalTranslatable;
use \AppBundle\Model\Objeto;

/**
 *
 * @ORM\Table(name="reserva_estado")
 * @ORM\Entity
 * 
*/
class ReservaEstado extends Objeto
{
    /** 
     * @ORM\ManyToOne(targetEntity="Reserva", inversedBy="estado") 
     * @ORM\JoinColumn(name="reserva_id", referencedColumnName="id", nullable=false) 
     */
    protected $reserva;
    
    /** 
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Estado\Reserva") 
     * @ORM\JoinColumn(name="estado_id", referencedColumnName="id", nullable=false) 
     */
    protected $estado;
    
    /**
     * @var \Date
     * 
     * @ORM\Column(name="fecha", type="date")
     */    
    protected $fecha;
    
    /** 
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Usuario") 
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id", nullable=false) 
     */
    protected $usuario;
    
    
}

