<?php

namespace AppBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Sonata\TranslationBundle\Model\Gedmo\TranslatableInterface;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Sonata\TranslationBundle\Traits\Gedmo\PersonalTranslatable;
use \AppBundle\Model\Objeto;

/**
 * @ORM\MappedSuperclass
*/
abstract class Estado
{    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;
    
    /**
     * @var string
     * 
     * @ORM\Column(name="nombre", type="text")
     */
    protected $nombre;
    
    /**
     * @var string
     * 
     * @ORM\Column(name="color", type="text")
     */
    protected $color;
    
    /**
     * @var bool
     * 
     * @ORM\Column(name="activa", type="boolean")
     */
    protected $activa;
    
    
    public function getId() {
        return $this->id;
    }
    
    public function getNombre() {
        return $this->nombre;
    }

    public function getColor() {
        return $this->color;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    public function setColor($color) {
        $this->color = $color;
    }
    
    public function __toString() {
        return $this->nombre;
    }
    
    public function getActiva() {
        return $this->activa;
    }

    public function setActiva($activa) {
        $this->activa = $activa;
        return $this;
    }
    
    public function setId($id) {
        $this->id = $id;
        return $this;
    }




}