<?php

/*
 * This file is part of the Sonata Project package.
 *
 * (c) Thomas Rabaix <thomas.rabaix@sonata-project.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;

use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;


/**
 * Class SonataAdminExtension.
 *
 * @author  Thomas Rabaix <thomas.rabaix@sonata-project.org>
 * @author  Michael Williams <michael.williams@funsational.com>
 */
class AppExtension extends Extension implements ExtensionInterface
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new YamlFileLoader(
            $container,
            new FileLocator(__DIR__.'/../Resources/config')
        );
        $loader->load('services.yml');
        //Las rutas no se pueden importar asi
        //$loader->load('routing.yml');
    }
}