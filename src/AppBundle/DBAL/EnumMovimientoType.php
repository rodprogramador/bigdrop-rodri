<?php
namespace AppBundle\DBAL;

class EnumMovimientoType extends EnumType
{
    protected $name = 'enummovimiento';
    protected $values = array('recogida', 'entrega', 'sustitucion');
    public function getValues() {
        return array('recogida'=>'recogida', 'entrega'=>'entrega', 'sustitucion'=>'sustitucion');
    }
}