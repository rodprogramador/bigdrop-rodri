<?php
namespace AppBundle\Twig;

class AppExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('hex2rgb', array($this, 'hex2rgbFilter')),
        );
    }

    public function priceFilter($number, $decimals = 0, $decPoint = '.', $thousandsSep = ',')
    {
        $price = number_format($number, $decimals, $decPoint, $thousandsSep);
        $price = '$'.$price;

        return $price;
    }
    
    public function hex2rgbFilter ( $hex_color ) {
        $values = str_replace( '#', '', $hex_color );
        switch ( strlen( $values ) ) {
            case 3;
                list( $r, $g, $b ) = sscanf( $values, "%1s%1s%1s" );
                return hexdec( "$r$r" ).",".hexdec( "$g$g" ) .",". hexdec( "$b$b" );
            case 6;
                return implode("," ,array_map( 'hexdec', sscanf( $values, "%2s%2s%2s" ) ) );
            default:
                return false;
        }
    }
    

    public function getName()
    {
        return 'app_extension';
    }
}
