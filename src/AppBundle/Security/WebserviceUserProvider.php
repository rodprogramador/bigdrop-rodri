<?php
// src/AppBundle/Security/User/WebserviceUserProvider.php
namespace AppBundle\Security;

use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Routing\RouterInterface;
use \Doctrine\ORM\EntityManager;

use AppBundle\Entity\Usuario;

class WebserviceUserProvider implements UserProviderInterface
{
    private $em;
    private $router;

    public function __construct(\Doctrine\ORM\EntityManagerInterface $em, RouterInterface $router)
    {        
        $this->em = $em;
        $this->router = $router;
    }

//    public function loadUserByCertificate($serial){
////        echo __CLASS__." ".__FUNCTION__;
//        $existingUser = $this->dm->getRepository('CredencialesBundle:Usuario')
//            ->findOneBy(['certificadoSerial' => $serial]);
//        if ($existingUser) {
//            return $existingUser;
//        }
//
//        return null;
//    }
    
    public function loadUserByEmail($email){
//        echo __FUNCTION__;
//        exit;
        
        $existingUser = $this->em->getRepository('AppBundle:Usuario')
            ->findOneBy(['email' => $email]);
        if ($existingUser) {
//            $existingUser->setEm($this->dm);
            return $existingUser;
        }
        
        return null;
    }

    public function loadUserByUsername($username)
    {
        return $this->loadUserByEmail($username);
    }

//    public function loadUserByOAuthUserResponse(UserResponseInterface $response)
//    {
//        echo __FUNCTION__;
//        exit;
//
//        $action = "oAuth".$response->getResourceOwner()->getName();
//        if(is_callable(array($this, $action))){
//            return $this->$action($response);
//        }else{
//            return null;
//        }
//    }

    public function refreshUser(UserInterface $user)
    {
        return $this->loadUserByUsername($user->getUsername());
//        return $user;
//        echo get_class($user).PHP_EOL;
////        if(!$user instanceof UserInterface){
////            echo 'NO ES INSTANCIA';
////        }
//        if (!($user instanceof UserInterface || (is_object($user) && method_exists($user, '__toString')) || is_string($user))) {
//            throw new \InvalidArgumentException('$user asdfasdfmust be an instanceof UserInterface, an object implementing a __toString method, or a primitive string.');
//        }        
//        echo $user->getUsername();
////        return $user;
//        echo __CLASS__." ".__FUNCTION__;
//
//        exit;
//        if (!$user instanceof WebserviceUser) {
//            return null;
//            throw new UnsupportedUserException(
//                sprintf('Instances of "%s" are not supported.', get_class($user))
//            );
//        }
//
//        return $this->loadUserByUsername($user->getUsername());
    }

    public function supportsClass($class)
    {
        return WebserviceUser::class === $class;
    }

//    protected function oAuthGoogle(UserResponseInterface $response)
//    {
////        echo $response->getEmail();
////        echo $response->getProfilePicture();
////        echo "<br>".$response->getRealName();
////        echo "<br>".$response->getExpiresIn();
////        echo "<br>".$response->getOAuthToken();
////        echo "<br>".$response->getAccessToken();
////        echo "<br>".$response->getTokenSecret();
////        echo "<br>".$response->getResourceOwner()->getName();
////        var_dump($response->getResponse());
//
//        $existingUser = $this->dm->getRepository('CredencialesBundle:Usuario')
//            ->findOneBy(['googleId' => $response->getResponse()['id']]);
//        if (!is_null($existingUser)) {
//            return $existingUser;
//        }
//        $usuario = $this->loadUserByEmail($response->getEmail());
//        if(is_null($usuario)){
//            $usuario = new \AppBundle\Entity\Usuario($this->dm);
//            $usuario->setEmail($response->getEmail());
//            $usuario->setNombre($response->getFirstName());
//            $usuario->setApellidos($response->getLastName());
//            $usuario->setUsername($response->getEmail());
//        }
//        $usuario->setGoogleId($response->getUsername());
//        $usuario->setGoogleToken($response->getAccessToken());
//
//        $this->dm->persist($usuario);
//        $this->dm->flush();
//
//        return $usuario;
//    }
}
