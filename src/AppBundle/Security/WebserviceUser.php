<?php
namespace AppBundle\Security;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\EquatableInterface;

class WebserviceUser implements UserInterface, EquatableInterface
{
    private $username;
    private $password;
    private $salt;
    private $roles;

    public function __construct($username, $password, $salt, array $roles)
    {
        echo __CLASS__." ".__FUNCTION__;
        exit;
        
        $this->username = $username;
        $this->password = $password;
        $this->salt = $salt;
        $this->roles = $roles;
    }

    public function getRoles()
    {
        echo __CLASS__." ".__FUNCTION__;
        exit;
        
        return $this->roles;
    }

    public function getPassword()
    {
        echo __CLASS__." ".__FUNCTION__;
        exit;
        
        return $this->password;
    }

    public function getSalt()
    {
        echo __CLASS__." ".__FUNCTION__;
        exit;
        
        return $this->salt;
    }

    public function getUsername()
    {
        echo __CLASS__." ".__FUNCTION__;
        exit;
        
        return $this->username;
    }

    public function eraseCredentials()
    {
        echo __CLASS__." ".__FUNCTION__;
        exit;
        
    }

    public function isEqualTo(UserInterface $user)
    {
        if (!$user instanceof WebserviceUser) {
            return false;
        }

        if ($this->password !== $user->getPassword()) {
            return false;
        }

        if ($this->salt !== $user->getSalt()) {
            return false;
        }

        if ($this->username !== $user->getUsername()) {
            return false;
        }

        return true;
    }
}