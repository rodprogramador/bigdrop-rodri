<?php
// src/AppBundle/Security/TokenAuthenticator.php
namespace AppBundle\Security;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\InMemoryUserProvider;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserProviderInterface;

use AppBundle\Entity\Usuario;

use Doctrine\ORM\EntityManager;

class FormAuthenticator extends AbstractGuardAuthenticator
{

  /**
   * @var \Symfony\Component\Routing\RouterInterface
   */
  private $router;

  /**
   *
   * @var \Doctrine\ORM\EntityManager
   */
  private $em;

  /**
   * Default message for authentication failure.
   *
   * @var string
   */
  private $failMessage = 'Invalid credentials';

  /**
   * Creates a new instance of FormAuthenticator
   */
  public function __construct(RouterInterface $router, \Doctrine\ORM\EntityManagerInterface $em) {
    $this->router = $router;
    $this->em = $em;
  }

  /**
   * {@inheritdoc}
   */
  public function getCredentials(Request $request)
  {      
    if ($request->getPathInfo() != '/login' || !$request->isMethod('POST')) {
      return;
    }
    $email = $request->request->get('login')['email'];
    $password = $request->request->get('login')['password'];

    if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
        throw new CustomUserMessageAuthenticationException("El email es incorrecto");
        return;
    }
    if(strlen($password)<6){
        throw new CustomUserMessageAuthenticationException("La contraseña debe tener al menos 6 caracteres");
        return;
    }

    return array(
      'email' => $email,
      'password' => $password,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getUser($credentials, UserProviderInterface $userProvider)
  {
      $usuario =  $userProvider->loadUserByEmail($credentials['email']);
      if(!$usuario){
          throw new CustomUserMessageAuthenticationException("El usuario no existe.");          
      }
      return $usuario;
  }

  /**
   * {@inheritdoc}
   */
  public function checkCredentials($credentials, UserInterface $user)
  {
    if ($user->verifyPassword($credentials['password'])) {
      return true;
    }
    throw new CustomUserMessageAuthenticationException("Password incorrecto");
    return false;
  }

  /**
   * {@inheritdoc}
   */
  public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
  {
    $url = $this->router->generate('panel_inicio');
    return new RedirectResponse($url);
  }

  /**
   * {@inheritdoc}
   */
  public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
  {
    $request->getSession()->set(Security::AUTHENTICATION_ERROR, $exception);
    $url = $this->router->generate('login');
    return new RedirectResponse($url);
  }

  /**
   * {@inheritdoc}
   */
  public function start(Request $request, AuthenticationException $authException = null)
  {
    $url = $this->router->generate('login');
    return new RedirectResponse($url);
  }

  /**
   * {@inheritdoc}
   */
  public function supportsRememberMe()
  {
    return true;
  }
}
