<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class DireccionType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('nombre', null, ['required'=> true])
                ->add('direccion', null, ['required' => true, 'constraints' => new Length(array('min' => 3))])
                ->add('cp')
                ->add('municipio')
                ->add('provincia', ChoiceType::class, ['choices' => array_flip(\AppBundle\Entity\Direccion::$provincias), 'attr' => ['class' => 'select2']])
                ->add('pais', ChoiceType::class, ['choices' => array_flip(\AppBundle\Entity\Direccion::$paises), 'attr' => ['class' => 'select2']])
                ->add('adicional')
                ->add('lat')
                ->add('lon')
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Direccion'
        ));
    }

}
