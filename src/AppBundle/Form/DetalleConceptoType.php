<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class DetalleConceptoType extends AbstractType
{

/**
* @param FormBuilderInterface $builder
* @param array $options
*/
public function buildForm(FormBuilderInterface $builder, array $options)
{

   $options['mapped'];
   $builder
          
        

                                      
   ->add('concepto', \Symfony\Component\Form\Extension\Core\Type\TextType::class, array(
    
    
    ))
    ->add('precio', \Symfony\Component\Form\Extension\Core\Type\MoneyType::class, array(
        ))
           
    ->add('detalle', ConceptoType::class, ['required' => true, 'constraints' => array
           (new \Symfony\Component\Validator\Constraints\Valid())])

    ->add('cantidad')
        ;
}

/**
* @param OptionsResolver $resolver
*/
public function configureOptions(OptionsResolver $resolver)
{
   $resolver->setDefaults(array(
       'data_class' => 'AppBundle\Entity\DetalleConcepto'
   ));
}

} 