<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MovimientoType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('fecha', \Symfony\Component\Form\Extension\Core\Type\DateTimeType::class, array(
                    'format' => 'dd/MM/yyyy HH:mm',
                    'widget' => 'single_text',
//                'dp_language' => 'es',
                    'attr' => array('class' => 'datetimepicker', 'movimiento' => $options['tipo']),
//                'dp_use_current' => false,
                    'label' => 'Fecha'
                        //'dp_min_date' => "function(){alert('looo'}",
                ))
//            ->add('direccion', \Symfony\Bridge\Doctrine\Form\Type\EntityType::class,
//                    [
//                        'class'=> \AppBundle\Entity\Direccion::class,
//                        'attr'=>array('class'=>'select2 select2'),
//                        'required' => false,
////                        'data' => $options['tipo']
//                    ]
//            )
        ;
//        if(isset($options['tipo']) && $options['tipo'] == "recodiga"){
//            $builder->add('direccion', \Iweb\GoogleMapsFormBundle\Form\Type\PlaceType::class,['include_gmaps_js'=>false]);
//        }else{
//            $builder->add('direccion', \Iweb\GoogleMapsFormBundle\Form\Type\PlaceType::class);
//        }
//        $builder->add('direccion', \Symfony\Component\Form\Extension\Core\Type\TextType::class);
        $builder->add('direccion', \Symfony\Bridge\Doctrine\Form\Type\EntityType::class, [
            'class' => \AppBundle\Entity\Direccion::class,
            'attr' => array('class' => 'select2'),
            'required' => true,
//                        'data' => $options['tipo']
                ]
        );

        $builder->add('estado', \Symfony\Bridge\Doctrine\Form\Type\EntityType::class, [
            'class' => \AppBundle\Entity\Estado\Movimiento::class,
            'attr' => array('class' => 'select2'),
            'required' => false,
//                        'data' => $options['tipo']
                ]
        );
        if (!isset($options['tipo'])) {
            $builder->add('tipo', \Symfony\Component\Form\Extension\Core\Type\TextType::class);
        } else {
            $builder->add('tipo', \Symfony\Component\Form\Extension\Core\Type\HiddenType::class, ['data' => $options['tipo']]);
        }
        if (!isset($options['reserva'])) {
            $builder->add('reserva', \Symfony\Bridge\Doctrine\Form\Type\EntityType::class, ['class' => \AppBundle\Entity\Reserva::class]);
        }

        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Movimiento',
            'tipo' => '',
            'reserva' => ''
        ));
        $resolver->setRequired('tipo');
        $resolver->setAllowedTypes('tipo', 'string');

        $resolver->setRequired('reserva');
        $resolver->setAllowedTypes('reserva', 'AppBundle\Entity\Reserva');
    }

}
