<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EmpresaType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('activa')
                ->add('sociedad')
                ->add('nombre')
                ->add('cif')
                ->add('pContacto')
                ->add('tlfFijo')
                ->add('tlfMovil')
                ->add('emailContacto')
                ->add('emailFacturacion', null, ['required' => false])
                ->add('web', null, ['required' => false])
                ->add('notasFacturacion', \Symfony\Component\Form\Extension\Core\Type\TextareaType::class, ['required' => false])
                ->add('dirFacturacion', DireccionType::class, ['required' => true, 'constraints' => array(new \Symfony\Component\Validator\Constraints\Valid())])
                ->add('dirEntrega', DireccionType::class, ['required' => true, 'constraints' => array(new \Symfony\Component\Validator\Constraints\Valid())])
                ->add('precioAlquiler', \Symfony\Component\Form\Extension\Core\Type\MoneyType::class, ['required' => false])
                ->add('precioTransporte', \Symfony\Component\Form\Extension\Core\Type\MoneyType::class, ['required' => false])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Empresa',
        ));
    }

}
