<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ReservaEmpresaType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $options['mapped'];
        $builder
                ->add('fechaInicio', \Symfony\Component\Form\Extension\Core\Type\DateTimeType::class, array(
                    'label' => 'Fecha Inicio.',
                    'format' => 'dd/MM/yyyy HH:mm',
                    'widget' => 'single_text',
//                    'dp_language' => 'es',
//                    'dp_use_current' => false,
//                    'dp_default_date' => date("d/m/Y 00:00"),
                    'attr' => array('altaFecha' => 'fechaInicio', 'class' => 'datetimepicker'),
                ))
                ->add('fechaFin', \Symfony\Component\Form\Extension\Core\Type\DateTimeType::class, array(
                    'label' => 'Fecha finalización.',
                    'format' => 'dd/MM/yyyy HH:mm',
                    'widget' => 'single_text',
//                    'dp_language' => 'es',
//                    'dp_use_current' => false,
//                    'dp_default_date' => date("d/m/Y 00:00"),
                    'attr' => array('altafecha' => 'fechaFin', 'class' => 'datetimepicker'),
                ))
                ->add('circuito', \Symfony\Component\Form\Extension\Core\Type\TextType::class, array(
                    'label' => 'Referencia'
                ))
                ->add('peticion', \Symfony\Component\Form\Extension\Core\Type\TextareaType::class, array(
                    'label' => 'Peticion del cliente'
                ))
                ->add('cantidadReceptores', \Symfony\Component\Form\Extension\Core\Type\IntegerType::class, array(
                ))
                ->add('cantidadEmisores', \Symfony\Component\Form\Extension\Core\Type\IntegerType::class, array(
                ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Reserva'
        ));
    }

}
