<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ConceptoType extends AbstractType
{
    
       
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
              //  ->add('cantidad', null, ['required'=> true])
                ->add('NroFactura')
                ->add('empresa', EntityType::class, array(
                    'class' => 'AppBundle:Empresa',
                    'choice_label' => 'sociedad',
                    'required' => false,
                    'attr' => array('class' => 'select2'),
                ))
                ->add('fecha', \Symfony\Component\Form\Extension\Core\Type\DateTimeType::class, array(
                    'label' => 'Fecha.',
                    'format' => 'dd/MM/yyyy HH:mm',
                    'widget' => 'single_text',
     //                    'dp_language' => 'es',
     //                    'dp_use_current' => false,
     //                    'dp_default_date' => date("d/m/Y 00:00"),
                    'attr' => array('altaFecha' => 'fecha', 'class' => 'datetimepicker'),
                ))
                ->add('base', \Symfony\Component\Form\Extension\Core\Type\MoneyType::class, array(
                    ))
                ->add('descuento', \Symfony\Component\Form\Extension\Core\Type\MoneyType::class, array(
                    ))
                ->add('iva', \Symfony\Component\Form\Extension\Core\Type\ChoiceType::class, array(
                    'block_name' => 'Impuestos a aplicar.',
                    'choices' => array(
                        'Standar 21%' => 21,
                        'Reducido 10%' => 10,
                        'Exento 0%' => 0
                    )
                ))
         
           
                ->add('total')
                
                
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Concepto'
        ));
    }
    
    }
    
