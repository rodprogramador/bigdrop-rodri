<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ReservaType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $options['mapped'];
        $builder
                ->add('fechaInicio', \Symfony\Component\Form\Extension\Core\Type\DateTimeType::class, array(
                    'label' => 'Fecha Inicio.',
                    'format' => 'dd/MM/yyyy HH:mm',
                    'widget' => 'single_text',
//                    'dp_language' => 'es',
//                    'dp_use_current' => false,
//                    'dp_default_date' => date("d/m/Y 00:00"),
                    'attr' => array('altaFecha' => 'fechaInicio', 'class' => 'datetimepicker'),
                ))
                ->add('fechaFin', \Symfony\Component\Form\Extension\Core\Type\DateTimeType::class, array(
                    'label' => 'Fecha finalización.',
                    'format' => 'dd/MM/yyyy HH:mm',
                    'widget' => 'single_text',
//                    'dp_language' => 'es',
//                    'dp_use_current' => false,
//                    'dp_default_date' => date("d/m/Y 00:00"),
                    'attr' => array('altafecha' => 'fechaFin', 'class' => 'datetimepicker'),
                ))
                ->add('empresa', EntityType::class, array(
                    'class' => 'AppBundle:Empresa',
                    'choice_label' => 'sociedad',
                    //'property' => array('sociedad','nombre','cif'),
                    'required' => true,
                    'attr' => array('class' => 'select2'),
                ))
                ->add('circuito', \Symfony\Component\Form\Extension\Core\Type\TextType::class, array(
                    'label' => 'Referencia'
                ))
//                ->add('estado','Symfony\Component\Form\Extension\Core\Type\ChoiceType',array(
//                    'label' => 'Estado de la reserva.',
//                    'choices' => array(
//                        'RG01' => 'Modelo Standar',
//                        'SAU' => 'Modelo Bluethoot'
//                    )
//                ))
                ->add('peticion', \Symfony\Component\Form\Extension\Core\Type\TextareaType::class, array(
                    'label' => 'Peticion del cliente'
                ))
                ->add('cantidadReceptores', \Symfony\Component\Form\Extension\Core\Type\IntegerType::class, array(
                ))
                ->add('cantidadEmisores', \Symfony\Component\Form\Extension\Core\Type\IntegerType::class, array(
                ))
                ->add('cantidadReserva', \Symfony\Component\Form\Extension\Core\Type\IntegerType::class, array(
                    'label' => 'Dispositivos reserva.'
                ))
                ->add('modelo', \Symfony\Component\Form\Extension\Core\Type\ChoiceType::class, array(
                    'label' => 'Tipo de aparatos.',
                    'choices' => array(
                        'RG01' => 'RG01',
                        'SAU' => 'SAU',
                        'RG17' => 'RG17',
                        'RGAA17' => 'RGAA17'
                    )
                ))
                ->add('precio', \Symfony\Component\Form\Extension\Core\Type\MoneyType::class, array(
                ))
                ->add('precioTipo', \Symfony\Component\Form\Extension\Core\Type\ChoiceType::class, array(
                    'block_name' => 'Forma de cobro.',
                    'choices' => array(
                        'Precio por aparato y dia' => 'simple',
                        'Precio por dia para todos los aparatos' => 'diario',
                        'Precio final de la reserva' => 'completo'
                    )
                ))
                ->add('precioTransporte', \Symfony\Component\Form\Extension\Core\Type\MoneyType::class, array(
                ))
                ->add('precioImpuestos', \Symfony\Component\Form\Extension\Core\Type\ChoiceType::class, array(
                    'block_name' => 'Impuestos a aplicar.',
                    'choices' => array(
                        'Standar 21%' => 21,
                        'Reducido 10%' => 10,
                        'Exento 0%' => 0
                    )
                ))
                ->add('entrega', MovimientoType::class, ['tipo' => 'entrega', 'reserva' => $options['data']])
                ->add('recogida', MovimientoType::class, ['tipo' => 'recogida', 'reserva' => $options['data']])
//                ->add('entregaFecha', \Symfony\Component\Form\Extension\Core\Type\DateTimeType::class, array(
//                    'format'  => 'dd/MM/yyyy HH:mm',
//                    'dp_language' => 'es',
//                    'attr'=>array('altafecha'=>'fechaEntrega'),
//                    'dp_use_current' => false,
//                    //'dp_min_date' => "function(){alert('looo'}",
//                ))
//                ->add('entregaDireccion', EntityType::class,array(
//                    'class' => 'AppBundle:Direccion',
//                    'choice_label' => 'nombre',
//                    //'property' => array('sociedad','nombre','cif'),
//                    'required' => false,
//                ))
//                ->add('entregaEstado','Symfony\Component\Form\Extension\Core\Type\ChoiceType',array(
////                    'query'        => $estadosReserva, //$query->getResult(),
////                    'template'  => 'AppBundle:Admin:list_estado.html.twig',
////                    'btn_add' => false
//                ))
//                ->add('recogidaFecha', \Symfony\Component\Form\Extension\Core\Type\DateTimeType::class, array(
//                    'format'  => 'dd/MM/yyyy HH:mm',
//                    'dp_language' => 'es',
//                    'attr'=>array('altafecha'=>'fechaRecogida'),
//                    'dp_use_current' => false,
//                    //'dp_min_date' => "function(){alert('looo'}",
//                ))
//                ->add('recogidaDireccion', EntityType::class,array(
//                    'class' => 'AppBundle:Direccion',
//                    'choice_label' => 'nombre',
//                    //'property' => array('sociedad','nombre','cif'),
//                    'required' => false,
//                ))
//                ->add('recogidaEstado', EntityType::class,array(
//                    'class' => 'AppBundle:Estado',
//                    'choice_label' => 'nombre',
//                    //'property' => array('sociedad','nombre','cif'),
//                    'required' => false,
//                ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Reserva'
        ));
    }

}
