<?php

namespace AppBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ReporteDiarioCommand extends Command
{

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    protected function configure()
    {
        $this
                // the name of the command (the part after "bin/console")
                ->setName('crm:reporte:juliatours')

                // the short description shown while running "php bin/console list"
                ->setDescription('Reporte diario Julia Tours.')

        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("Starting the cron");
        $this->em = $this->getApplication()->getKernel()->getContainer()->get("doctrine")->getManager();

        //Reservas con estado presupuesto que estén pasadas de fecha.
        $julia = $this->em->getRepository(\AppBundle\Entity\Empresa::class)->find(152);
        $query = $this->em->getRepository(\AppBundle\Entity\Reserva::class)->createQueryBuilder('r')
                ->andWhere('r.empresa = :empresa')
                ->setParameter('empresa', $julia);

        $query->andWhere("r.fechaAlta >= :inicio ")
                ->andWhere("r.fechaAlta <= :fin ")
                ->setParameter("inicio", \DateTime::createFromFormat("d-m-Y_H:i:s", date("d") . "-" . date("m") . "-" . date("Y") . "_00:00:00"))
                ->setParameter("fin", \DateTime::createFromFormat("d-m-Y_H:i:s", date("d") . "-" . date("m") . "-" . date("Y") . "_23:59:59"));
        $reservas = $query->getQuery()->getResult();

        $message = (new \Swift_Message('Reporte diario Julia tours'))
                ->setFrom('info@rgspain.com')
                ->setTo('rg@rgspain.com')
                ->setBody($this->getApplication()->getKernel()->getContainer()->get('templating')->render('@Admin/Email/ReporteJulia.html.twig', ['reservas' => $reservas]), 'text/html')
        ;
        $this->getApplication()->getKernel()->getContainer()->get('mailer')->send($message);

        $output->writeln("Cron completed");
    }

}
