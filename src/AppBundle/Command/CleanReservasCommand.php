<?php

namespace AppBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CleanReservasCommand extends Command
{

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    protected function configure()
    {
        $this
                // the name of the command (the part after "bin/console")
                ->setName('reservas:clean')

                // the short description shown while running "php bin/console list"
                ->setDescription('Limpia reservas.')

        ;
    }

    /**
     * 1º Reservas con estado presupuesto y que haya pasado la fecha de inicio las pasa a canceladas.
     * 2º Los movimientos no finalizados en reservas canceladas los pone en finalizados.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("Starting the cron");
        $this->em = $this->getApplication()->getKernel()->getContainer()->get("doctrine")->getManager();

        //Reservas con estado presupuesto que estén pasadas de fecha.
        $er_presu = $this->em->getRepository(\AppBundle\Entity\Estado\Reserva::class)->findOneByNombre("PRESUPUESTO");
        $er_cancel = $this->em->getRepository(\AppBundle\Entity\Estado\Reserva::class)->findOneByNombre("CANCELADO");

        $reservas = $this->em->getRepository(\AppBundle\Entity\Reserva::class)->findByEstado($er_presu);
        $canceladas = 0;
        foreach ($reservas as $reserva) {
            /* @var $reserva \AppBundle\Entity\Reserva */
            if ($reserva->getFechaInicio() < new \DateTime("-1day")) {
                $reserva->setEstado($er_cancel);
                $this->em->persist($reserva);
                $canceladas++;
            }
        }
        $output->writeln("Reservas " . $canceladas);

        $em_fin = $this->em->getRepository(\AppBundle\Entity\Estado\Movimiento::class)->findOneByNombre("FINALIZADO");
        $reservas = $this->em->getRepository(\AppBundle\Entity\Reserva::class)->findByEstado($er_cancel);
        $canceladas = 0;
        foreach ($reservas as $reserva) {
            /* @var $reserva \AppBundle\Entity\Reserva */
            $reserva->getEntrega()->setEstado($em_fin);
            $reserva->getRecogida()->setEstado($em_fin);
            $this->em->persist($reserva->getEntrega());
            $this->em->persist($reserva->getRecogida());
            $canceladas++;
        }
        $output->writeln("Movimientos " . $canceladas);


        $this->em->flush();
//        $output->writeln("Cuentas eliminadas " . $eliminado);
        $output->writeln("Cron completed");
    }

}
