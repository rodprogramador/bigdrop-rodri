/* 
 * The MIT License
 *
 * Copyright 2016 Víctor García <vgpastor@ingenierosweb.co>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

$().ready(function(){
    AltaRegistro();
});
function AltaRegistro(){
    var fechaInicio = $('input[altafecha="fechaInicio"]');
    var fechaFin = $('input[altafecha="fechaFin"]');
    var fechaEntrega = $('input[movimiento="entrega"]');
    var fechaRecogida = $('input[movimiento="recogida"]');
    
    
    $(fechaInicio).on("dp.change", function (e) {
        if(e.date>=$(fechaFin).data("DateTimePicker").date()){
            $(fechaFin).data("DateTimePicker").date(e.date);
        }
        $(fechaFin).data("DateTimePicker").minDate(e.date);
        
        if(e.date<$(fechaEntrega).data("DateTimePicker").date()){
            $(fechaEntrega).data("DateTimePicker").date(e.date)
        }
        $(fechaEntrega).data("DateTimePicker").maxDate(e.date);
        ActualizaDias();
    });
    
    $(fechaFin).on("dp.change", function (e) {
        $(fechaRecogida).data("DateTimePicker").minDate(e.date);
        if($(fechaRecogida).data("DateTimePicker").date()<=e.date){
            $(fechaRecogida).data("DateTimePicker").date(e.date);
        }
        ActualizaDias();
    });
    
    function ActualizaDias(){
        tot = $(fechaFin).data("DateTimePicker").date() - $(fechaInicio).data("DateTimePicker").date();
        tot = Math.floor((tot/86400000)+1);
        $("#totaldias").val(tot);
    }
    
    
    fechaEntrega.on('blur',function() {
        inicio = new Date(fechaInicio.val());
        entrega = new Date(fechaEntrega.val());
          if(entrega >= inicio){
              alert("La fecha de entrega no puede ser anterior a la de inicio");
          }
    });
    
    fechaRecogida.on('blur',function() {
        fin = new Date(fechaFin.val());
        recogida = new Date(fechaRecogida.val());
          if(fin >= recogida){
              alert("La fecha de recogida no puede ser anterior a la de finalización");
          }
    });
    ActualizaDias();
}

function addEmpresa(){
    $("#MMmodal").find(".modal-title").html("Nueva empresa");
    $("#MMmodal").find(".modal-body").html("");
    $("#MMmodal").modal("show");
    
}
