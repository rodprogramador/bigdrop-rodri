/* 
 * The MIT License
 *
 * Copyright 2016 Víctor García <vgpastor@ingenierosweb.co>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

$().ready(function(){
    AltaRegistro();
});

function AltaRegistro(){
    var fechaInicio = $('input[altafecha="fechaInicio"]');
    var fechaFin = $('input[altafecha="fechaFin"]');    
    
    $(fechaInicio).on("dp.change", function (e) {
        if(e.date>=$(fechaFin).data("DateTimePicker").date()){
            $(fechaFin).data("DateTimePicker").date(e.date);
        }
        $(fechaFin).data("DateTimePicker").minDate(e.date);
        
        ActualizaDias();
    });
    
    $(fechaFin).on("dp.change", function (e) {
        ActualizaDias();
    });
    
    function ActualizaDias(){
        tot = $(fechaFin).data("DateTimePicker").date() - $(fechaInicio).data("DateTimePicker").date();
        tot = Math.floor((tot/86400000)+1);
        console.log($(fechaFin).data("DateTimePicker").date());
        console.log($(fechaInicio).data("DateTimePicker"));
        $("#totaldias").val(tot);
    }
    window.setTimeout( ActualizaDias, 1500 ); 
}
