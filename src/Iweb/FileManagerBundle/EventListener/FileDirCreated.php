<?php
namespace Iweb\FileManagerBundle\EventListener;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Iweb\FileManagerBundle\Events\FileEvent;
use Iweb\FileManagerBundle\IwebFileManagerEvents;

/**
 * Class FileDirCreated
 * @package Youwe\FileManagerBundle\EventListener
 */
class FileDirCreated implements EventSubscriberInterface
{
    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(
            IwebFileManagerEvents::BEFORE_FILE_DIR_CREATED => 'beforeFileDirCreated',
            IwebFileManagerEvents::AFTER_FILE_DIR_CREATED  => 'afterFileDirCreated',
        );
    }

    /**
     * @param FileEvent $event
     */
    public function afterFileDirCreated(FileEvent $event)
    {
    }

    /**
     * @param FileEvent $event
     */
    public function beforeFileDirCreated(FileEvent $event)
    {
    }

}