<?php
namespace Iweb\FileManagerBundle\EventListener;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Iweb\FileManagerBundle\Events\FileEvent;
use Iweb\FileManagerBundle\IwebFileManagerEvents;

/**
 * Class FileUploaded
 * @package Youwe\FileManagerBundle\EventListener
 */
class FileUploaded implements EventSubscriberInterface
{
    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(
            IwebFileManagerEvents::AFTER_FILE_UPLOADED  => 'afterFileUploaded',
            IwebFileManagerEvents::BEFORE_FILE_UPLOADED => 'beforeFileUploaded',
        );
    }

    /**
     * @param FileEvent $event
     */
    public function afterFileUploaded(FileEvent $event)
    {
    }

    /**
     * @param FileEvent $event
     */
    public function beforeFileUploaded(FileEvent $event)
    {
    }
}