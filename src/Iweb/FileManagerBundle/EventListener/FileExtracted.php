<?php
namespace Iweb\FileManagerBundle\EventListener;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Iweb\FileManagerBundle\Events\FileEvent;
use Iweb\FileManagerBundle\IwebFileManagerEvents;

/**
 * Class FileExtracted
 * @package Youwe\FileManagerBundle\EventListener
 */
class FileExtracted implements EventSubscriberInterface
{
    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(
            IwebFileManagerEvents::AFTER_FILE_EXTRACTED  => 'afterFileExtracted',
            IwebFileManagerEvents::BEFORE_FILE_EXTRACTED => 'beforeFileExtracted',
        );
    }

    /**
     * @param FileEvent $event
     */
    public function afterFileExtracted(FileEvent $event)
    {
    }

    /**
     * @param FileEvent $event
     */
    public function beforeFileExtracted(FileEvent $event)
    {
    }
}