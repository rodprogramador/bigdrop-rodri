<?php
namespace Iweb\FileManagerBundle\EventListener;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Iweb\FileManagerBundle\Events\FileEvent;
use Iweb\FileManagerBundle\IwebFileManagerEvents;

/**
 * Class FilePasted
 * @package Youwe\FileManagerBundle\EventListener
 */
class FilePasted implements EventSubscriberInterface
{
    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(
            IwebFileManagerEvents::AFTER_FILE_PASTED  => 'afterFilePasted',
            IwebFileManagerEvents::BEFORE_FILE_PASTED => 'beforeFilePasted',
        );
    }

    /**
     * @param FileEvent $event
     */
    public function afterFilePasted(FileEvent $event)
    {
    }

    /**
     * @param FileEvent $event
     */
    public function beforeFilePasted(FileEvent $event)
    {
    }
}