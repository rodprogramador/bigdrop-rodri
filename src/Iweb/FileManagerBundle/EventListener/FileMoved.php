<?php
namespace Iweb\FileManagerBundle\EventListener;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Iweb\FileManagerBundle\Events\FileEvent;
use Iweb\FileManagerBundle\IwebFileManagerEvents;

/**
 * Class FileMoved
 * @package Youwe\FileManagerBundle\EventListener
 */
class FileMoved implements EventSubscriberInterface
{
    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(
            IwebFileManagerEvents::AFTER_FILE_MOVED  => 'afterFileMoved',
            IwebFileManagerEvents::BEFORE_FILE_MOVED => 'beforeFileMoved',
        );
    }

    /**
     * @param FileEvent $event
     */
    public function afterFileMoved(FileEvent $event)
    {
    }

    /**
     * @param FileEvent $event
     */
    public function beforeFileMoved(FileEvent $event)
    {
    }
}