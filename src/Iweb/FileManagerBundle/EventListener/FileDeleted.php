<?php
namespace Iweb\FileManagerBundle\EventListener;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Iweb\FileManagerBundle\Events\FileEvent;
use Iweb\FileManagerBundle\IwebFileManagerEvents;

/**
 * Class FileDeleted
 * @package Youwe\FileManagerBundle\EventListener
 */
class FileDeleted implements EventSubscriberInterface
{
    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(
            IwebFileManagerEvents::AFTER_FILE_DELETED  => 'afterFileDeleted',
            IwebFileManagerEvents::BEFORE_FILE_DELETED => 'beforeFileDeleted',
        );
    }

    /**
     * @param FileEvent $event
     */
    public function afterFileDeleted(FileEvent $event)
    {
    }

    /**
     * @param FileEvent $event
     */
    public function beforeFileDeleted(FileEvent $event)
    {
    }
}