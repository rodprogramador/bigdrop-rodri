<?php
namespace Iweb\FileManagerBundle\EventListener;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Iweb\FileManagerBundle\Events\FileEvent;
use Iweb\FileManagerBundle\IwebFileManagerEvents;

/**
 * Class FileRenamed
 * @package Youwe\FileManagerBundle\EventListener
 */
class FileRenamed implements EventSubscriberInterface
{
    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(
            IwebFileManagerEvents::AFTER_FILE_RENAMED  => 'afterFileRenamed',
            IwebFileManagerEvents::BEFORE_FILE_RENAMED => 'beforeFileRenamed',
        );
    }

    /**
     * @param FileEvent $event
     */
    public function afterFileRenamed(FileEvent $event)
    {
    }

    /**
     * @param FileEvent $event
     */
    public function beforeFileRenamed(FileEvent $event)
    {
    }
}