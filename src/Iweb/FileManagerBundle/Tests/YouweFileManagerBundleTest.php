<?php

namespace Iweb\FileManager\Test;

use Iweb\FileManagerBundle\IwebFileManagerBundle;

/**
 * Class IwebFileManagerBundleTest
 * @package Youwe\FileManager\Test
 */
class IwebFileManagerBundleTest extends \PHPUnit_Framework_TestCase
{
    public function testIwebFileManagerBundle()
    {
        $bundle = new IwebFileManagerBundle();

        $this->assertInstanceOf('Symfony\Component\HttpKernel\Bundle\Bundle', $bundle);
    }
}
