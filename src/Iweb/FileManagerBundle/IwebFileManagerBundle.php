<?php

namespace Iweb\FileManagerBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class IwebFileManagerBundle
 * @package Iweb\FileManagerBundle
 */
class IwebFileManagerBundle extends Bundle
{
}
