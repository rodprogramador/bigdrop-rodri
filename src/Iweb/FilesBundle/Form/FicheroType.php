<?php

namespace Iweb\FilesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FicheroType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('file', 'file', array(
            'required' => false,
            'attr'     => array('class' => 'form-control file_manager_url', 'multiple' => 'multiple'),
            'label'    => 'Files'
        ));

        $builder
            ->add('nombre')
            ->add('path')
            ->add('type')
            ->add('entidad')
            ->add('idEntidad')
            ->add('categoria')
            ->add('fechaSubida', 'datetime')
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Iweb\FilesBundle\Entity\Fichero'
        ));
    }
}
