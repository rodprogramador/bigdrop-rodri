<?php

/*
 * The MIT License
 *
 * Copyright 2016 Víctor García <vgpastor@ingenierosweb.co>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Iweb\FilesBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Iweb\FilesBundle\Model\Objeto;

/**
 * Fichero
 *
 * @ORM\Table(name="fichero")
 * @ORM\Entity(repositoryClass="Iweb\FilesBundle\Repository\FicheroRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Fichero extends Objeto
{

    /**
     * @var String
     *
     * @ORM\Column(name="nombre", type="string")
     */
    protected $nombre;

    /**
     * @var String
     *
     * @ORM\Column(name="path", type="string")
     */
    protected $path;

    /**
     * @var String
     *
     * @ORM\Column(name="type", type="string")
     */
    protected $type;

    /**
     * @var String
     *
     * @ORM\Column(name="entidad", type="string", nullable=true)
     */
    protected $entidad;

    /**
     * @var Integer
     *
     * @ORM\Column(name="idEntidad", type="integer", nullable=true)
     */
    protected $idEntidad;

    /**
     * @var String
     *
     * @ORM\Column(name="categoria", type="string", nullable=true)
     */
    protected $categoria;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaSubida", type="datetime")
     */
    protected $fechaSubida;

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function prePersist()
    {
        $this->fechaSubida = new \DateTime();
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function getPath()
    {
        return $this->path;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getEntidad()
    {
        return $this->entidad;
    }

    public function getIdEntidad()
    {
        return $this->idEntidad;
    }

    public function getCategoria()
    {
        return $this->categoria;
    }

    public function getFechaSubida()
    {
        return $this->fechaSubida;
    }

    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    public function setPath($path)
    {
        $this->path = $path;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function setEntidad($entidad)
    {
        $this->entidad = $entidad;
    }

    public function setIdEntidad($idEntidad)
    {
        $this->idEntidad = $idEntidad;
    }

    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;
    }

    public function setFechaSubida(\DateTime $fechaSubida)
    {
        $this->fechaSubida = $fechaSubida;
    }

    function upload(\Symfony\Component\HttpFoundation\File\File $fichero, $folder)
    {
        $this->path = time() . $this->string_sanitize($fichero->getClientOriginalName()) . "." . $fichero->guessExtension();
        $this->setType($fichero->getMimeType());
        $this->setNombre($fichero->getClientOriginalName());
        $fichero->move($folder, $this->path);

        return $this;
    }

    private function string_sanitize($string, $force_lowercase = true, $anal = false)
    {
        $strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]",
            "}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
            "â€�?", "â€“", ",", "<", ".", ">", "/", "?");
        $clean = trim(str_replace($strip, "", strip_tags($string)));
        $clean = preg_replace('/\s+/', "-", $clean);
        $clean = ($anal)?preg_replace("/[^a-zA-Z0-9]/", "", $clean):$clean;
        return ($force_lowercase)?
                (function_exists('mb_strtolower'))?
                mb_strtolower($clean, 'UTF-8'):
                strtolower($clean):
                $clean;
    }

    /**
     * @ORM\PreRemove
     */
    public function delete()
    {
        if (
                file_exists(__DIR__ . '/../../../../web/uploads/' . $this->path) && is_writable(__DIR__ . '/../../../../web/uploads/' . $this->path)
        ) {
            unlink(__DIR__ . '/../../../../web/uploads/' . $this->path);
        } else {
            throw new \Exception(__DIR__ . '/../../../../web/uploads/' . $this->path);
        }
    }

}
