<?php

namespace Iweb\FilesBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Iweb\FilesBundle\Entity\Fichero;
use Iweb\FilesBundle\Form\FicheroType;

/**
 * Fichero controller.
 *
 * @Route("/fichero")
 */
class FicheroController extends Controller
{
    /**
     * Lists all Fichero entities.
     *
     * @Route("/", name="fichero_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $ficheroes = $em->getRepository('IwebFilesBundle:Fichero')->findAll();

        return $this->render('IwebFilesBundle:Default:index.html.twig', array(
            'ficheroes' => $ficheroes,
        ));
    }

    /**
     * Creates a new Fichero entity.
     *
     * @Route("/new", name="fichero_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $fichero = new Fichero();
        $form = $this->createForm('Iweb\FilesBundle\Form\FicheroType', $fichero);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($fichero);
            $em->flush();

            return $this->redirectToRoute('fichero_show', array('id' => $fichero->getId()));
        }

        return $this->render('IwebFilesBundle:Default:new.html.twig', array(
            'fichero' => $fichero,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Fichero entity.
     *
     * @Route("/{id}", name="fichero_show")
     * @Method("GET")
     */
    public function showAction(Fichero $fichero)
    {
        $deleteForm = $this->createDeleteForm($fichero);

        return $this->render('IwebFilesBundle:Default:show.html.twig', array(
            'fichero' => $fichero,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Fichero entity.
     *
     * @Route("/{id}/edit", name="fichero_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Fichero $fichero)
    {
        $deleteForm = $this->createDeleteForm($fichero);
        $editForm = $this->createForm('Iweb\FilesBundle\Form\FicheroType', $fichero);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($fichero);
            $em->flush();

            return $this->redirectToRoute('fichero_edit', array('id' => $fichero->getId()));
        }

        return $this->render('IwebFilesBundle:Default:edit.html.twig', array(
            'fichero' => $fichero,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Fichero entity.
     *
     * @Route("/{id}", name="fichero_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Fichero $fichero)
    {
        $form = $this->createDeleteForm($fichero);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($fichero);
            $em->flush();
        }

        return $this->redirectToRoute('fichero_index');
    }

    /**
     * Creates a form to delete a Fichero entity.
     *
     * @param Fichero $fichero The Fichero entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Fichero $fichero)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('fichero_delete', array('id' => $fichero->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
