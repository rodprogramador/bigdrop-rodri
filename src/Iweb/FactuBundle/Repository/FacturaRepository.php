<?php

namespace Iweb\FactuBundle\Repository;

/**
 * FacturaRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class FacturaRepository extends \Doctrine\ORM\EntityRepository
{

    public function findByEstado($estado, \AppBundle\Entity\Usuario $comercial = null)
    {
        $query = $this->createQueryBuilder('f')
                ->leftJoin("f.estado", "e")
                ->where('e.nombre = :estado')
                ->setParameter("estado", $estado);

        if (!is_null($comercial)) {
            $query->andWhere('f.emisor = :empresa OR f.receptor = :empresa')
                    ->setParameter('empresa', $comercial->getEmpresa());
        }

        $facturas = $query->getQuery()->getResult();
        return $facturas;
    }

    public function findAll(\AppBundle\Entity\Usuario $comercial = null)
    {
        $query = $this->createQueryBuilder('f');

        if (!is_null($comercial)) {
            $query->andWhere('f.emisor = :empresa OR f.receptor = :empresa')
                    ->setParameter('empresa', $comercial->getEmpresa());
        }

        $facturas = $query->getQuery()->getResult();
        return $facturas;
    }

    public function totalMonths($year)
    {
        $query = $this->createQueryBuilder('f')
                ->where('b.created BETWEEN :start AND :end')
                ->setParameter('start', $date->format('Y-m-d'))
                ->setParameter('end', $date->format('Y-m-t'))
                ->addSelect('sum(f.base) as base')
                ->groupBy('f.emisor')
        ;

        if (!is_null($comercial)) {
            $query->andWhere('f.emisor = :empresa OR f.receptor = :empresa')
                    ->setParameter('empresa', $comercial->getEmpresa());
        }

        $facturas = $query->getQuery()->getResult();
        return $facturas;
    }

    public function totalMonth($year, $month)
    {
        $date = new \DateTime("{$year}-{$month}-01");

        $qb = $this->createQueryBuilder('f');
        $query = $qb
                ->addSelect('sum(f.base) as base')
                ->groupBy('f.emisor')
                ->where('f.fechaEmision BETWEEN :start AND :end')
                ->setParameter('start', $date->format('Y-m-d'))
                ->setParameter('end', $date->format('Y-m-t'))
        ;
        return $query->getQuery()->getResult();
    }

}
