<?php

namespace Iweb\FactuBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class AlertaCosteCommand extends Command
{

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    protected function configure()
    {
        $this
                // the name of the command (the part after "bin/console")
                ->setName('alerta:coste')

                // the short description shown while running "php bin/console list"
                ->setDescription('Lanza un email con las alertas de los costes pendientes.')

        ;
    }

    /**
     * 1º Reservas con estado presupuesto y que haya pasado la fecha de inicio las pasa a canceladas.
     * 2º Los movimientos no finalizados en reservas canceladas los pone en finalizados.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("Starting the cron");
        $this->em = $this->getApplication()->getKernel()->getContainer()->get("doctrine")->getManager();

        $costes = $this->em->getRepository(\Iweb\FactuBundle\Entity\FacturaRecibida::class)->findProximas();
        $alertas = [];
        foreach ($costes as $coste) {
            if ($coste->getAlerta() == 0 || $coste->getFechaVencimiento() == null) {
                continue;
            }
            $difference = round(abs((new \DateTime())->getTimestamp() - $coste->getFechaVencimiento()->getTimestamp()) / 3600);
            echo $coste->getId() . " " . $coste->getAlerta() . " " . $coste->getFechaVencimiento()->format("d/m/Y H:i:s") . " " . $difference . PHP_EOL;

            if ($difference < $coste->getAlerta()) {
                echo "Alerta " . $coste->getId() . PHP_EOL;
                $coste->setAlerta(0);
                $this->em->persist($coste);
                $alertas[] = "Alerta vencimiento: Coste ID " . $coste->getId() . " Importe:" . $coste->getBase() . " Fecha:" . $coste->getFechaVencimiento()->format("d/m/Y H:i:s");
            }
        }
        if (count($alertas) >= 1) {
            $mailer = $this->getApplication()->getKernel()->getContainer()->get("mailer");
            $message = (new \Swift_Message('Alertas vencimiento costes'))
                    ->setFrom('info@rgspain.com')
                    ->setTo('rg@rgspain.com')
                    ->setBody(implode("<br />", $alertas), 'text/html')
            ;

            $mailer->send($message);
        }
        $this->em->flush();
        $output->writeln("Cron completed");
    }

}
