<?php

namespace Iweb\FactuBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class CosteType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('emisor', EntityType::class, array(
                    'class' => 'AppBundle:Empresa',
                    'choice_label' => 'sociedad',
                    //'property' => array('sociedad','nombre','cif'),
                    'required' => true,
                    'attr' => array('class' => 'select2'),
                ))
                ->add('numero', null, ['label' => 'Nº Factura'])
                ->add('categoria', \Symfony\Component\Form\Extension\Core\Type\ChoiceType::class, array(
                    'block_name' => 'Categoria del gasto.',
                    'choices' =>
                    ["General" => [
                            "Bancos" => "Bancos",
                            "Impuestos" => "Impuestos",
                            "Salarios" => "Salarios",
                            "TGSS" => "TGSS"
                        ],
                        "Explotación" => [
                            "Oficina" => "Oficina",
                            "Explotación" => "Explotación",
                            "Importaciones" => "Importaciones"
                        ]
                    ]
                ))
                ->add('fechaEmision', \Symfony\Component\Form\Extension\Core\Type\DateTimeType::class, array(
                    'label' => 'Fecha.',
                    'format' => 'dd/MM/yyyy',
                    'widget' => 'single_text',
                    'attr' => array('altaFecha' => 'fecha', 'class' => 'datepicker'),
                ))
                ->add('fechaVencimiento', \Symfony\Component\Form\Extension\Core\Type\DateTimeType::class, array(
                    'format' => 'dd/MM/yyyy',
                    'widget' => 'single_text',
                    'attr' => array('altaFecha' => 'fecha', 'class' => 'datepicker'),
                ))
                ->add('fechaPago', \Symfony\Component\Form\Extension\Core\Type\DateTimeType::class, array(
                    'format' => 'dd/MM/yyyy',
                    'widget' => 'single_text',
                    'attr' => array('altaFecha' => 'fecha', 'class' => 'datepicker'),
                    'required' => false
                ))
                ->add('alerta', null, ['label' => 'Alerta Nº horas'])
                ->add('estado', EntityType::class, array(
                    'class' => \Iweb\FactuBundle\Entity\Estado::class,
                    'choice_label' => 'nombre',
                    //'property' => array('sociedad','nombre','cif'),
                    'required' => true,
                    'attr' => array('class' => 'select2'),
                ))
                ->add('base')
                ->add('impuestos', \Symfony\Component\Form\Extension\Core\Type\ChoiceType::class, array(
                    'block_name' => 'Impuestos a aplicar.',
                    'choices' => array(
                        'Standar 21%' => 21,
                        'Reducido 10%' => 10,
                        'Exento 0%' => 0
                    )
                ))
                ->add('notas')
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Iweb\FactuBundle\Entity\FacturaRecibida'
        ));
    }

}
