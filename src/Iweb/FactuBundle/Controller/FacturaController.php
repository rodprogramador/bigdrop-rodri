<?php

namespace Iweb\FactuBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Iweb\FactuBundle\Entity\Factura;
use Iweb\FactuBundle\Form\FacturaType;
use Symfony\Component\HttpFoundation\Response;

/**
 * Factura controller.
 *
 * @Route("/factura")
 */
class FacturaController extends Controller
{

    /**
     * Creates a new Factura entity.
     *
     * @Route("/new", name="factura_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $factura = new Factura();
        $form = $this->createForm('Iweb\FactuBundle\Form\FacturaType', $factura);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($factura);
            $em->flush();

            return $this->redirectToRoute('factura_show', array('id' => $factura->getId()));
        }

        return $this->render('IwebFactuBundle:Factura:new.html.twig', array(
                    'factura' => $factura,
                    'form' => $form->createView(),
        ));
    }

    /**
     *
     * @Route("/{id}", name="factura_print")
     * @Method("GET")
     */
    public function printAction(Factura $factura)
    {
        $html = $this->renderView('IwebFactuBundle:Factura:PDF-A4.html.twig', array(
            'factura' => $factura
        ));

        return new Response(
                $this->get('knp_snappy.pdf')->getOutputFromHtml($html), 200, array(
            'Content-Type' => 'application/pdf',
//                'Content-Disposition'   => 'attachment; filename="RGSpain_Factura_F'.$reserva->getId().'.pdf"'
                )
        );
    }

    /**
     * Finds and displays a Factura entity.
     *
     * @Route("/{id}", name="factura_show")
     * @Method("GET")
     */
    public function showAction(Factura $factura)
    {
        $deleteForm = $this->createDeleteForm($factura);

        return $this->render('IwebFactuBundle:Factura:show.html.twig', array(
                    'factura' => $factura,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Factura entity.
     *
     * @Route("/{id}/edit", name="factura_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Factura $factura)
    {
        $deleteForm = $this->createDeleteForm($factura);
        $editForm = $this->createForm('Iweb\FactuBundle\Form\FacturaType', $factura);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($factura);
            $em->flush();

            return $this->redirectToRoute('factura_edit', array('id' => $factura->getId()));
        }

        return $this->render('IwebFactuBundle:Factura:edit.html.twig', array(
                    'factura' => $factura,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Factura entity.
     *
     * @Route("/{id}", name="factura_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Factura $factura)
    {
        $form = $this->createDeleteForm($factura);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($factura);
            $em->flush();
        }

        return $this->redirectToRoute('factura_index');
    }

    /**
     * Creates a form to delete a Factura entity.
     *
     * @param Factura $factura The Factura entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Factura $factura)
    {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('factura_delete', array('id' => $factura->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

    /**
     *
     * @Route("/{id}/estado", name="factura_estado")
     * @Method("GET")
     */
    public function estadoAction(Factura $factura)
    {
        $em = $this->getDoctrine()->getManager();

        $pedidos = $factura->getPedido();
        $epagada = $this->getDoctrine()->getRepository(\Iweb\FactuBundle\Entity\Estado::class)->findOneByNombre("PAGADA");
        $efacturada = $this->getDoctrine()->getRepository(\Iweb\FactuBundle\Entity\Estado::class)->findOneByNombre("FACTURADA");
        $ereembolso = $this->getDoctrine()->getRepository(\Iweb\FactuBundle\Entity\Estado::class)->findOneByNombre("REEMBOLSO");

        $mfinalizado = $this->getDoctrine()->getRepository(\AppBundle\Entity\Estado\Movimiento::class)->findOneByNombre("FINALIZADO");

        $rconfirmado = $this->getDoctrine()->getRepository(\AppBundle\Entity\Estado\Reserva::class)->findOneByNombre("CONFIRMADO");
        $rfinalizada = $this->getDoctrine()->getRepository(\AppBundle\Entity\Estado\Reserva::class)->findOneByNombre("FINALIZADA");

        $repoEstado = $this->getDoctrine()->getRepository('AppBundle:Estado');
        if ($factura->getEstado() == $epagada) {
            $factura->setEstado($ereembolso);
        } elseif ($factura->getEstado() == $efacturada) {
            $factura->setEstado($epagada);
            foreach ($factura->getPedido() as $reserva) {
                if ($reserva->getEntregaEstado() == $mfinalizado && $reserva->getRecogidaEstado() == $mfinalizado && $reserva->getEstado() == $rconfirmado) {
                    $reserva->setEstado($rfinalizada);
                }
            }
        } elseif ($factura->getEstado() == $ereembolso) {
            $factura->setEstado($efacturada);
        }
        $em->persist($factura);
        $em->flush();


        return $this->redirectToRoute('facturacion_index');
    }

    /**
     *
     * @Route("/{id}/borrar", name="factura_borrar")
     * @Method("GET")
     */
    public function borrarAction(Factura $factura)
    {
        $efacturada = $this->getDoctrine()->getRepository(\Iweb\FactuBundle\Entity\Estado::class)->findOneByNombre("FACTURADA");
        if ($factura->getEstado() == $efacturada) {
            $em = $this->getDoctrine()->getManager();
            foreach ($factura->getPedido() as $pedido) {
                $pedido->setFactura(null);
//                $pedido->setEstado($em->getRepository('AppBundle:Estado')->findOneBy(['nombre' => 'ACEPTADO']));
                $em->persist($pedido);
            }
            $em->remove($factura);
            $em->flush();
        }
        return $this->redirectToRoute('facturacion_index');
    }

}
