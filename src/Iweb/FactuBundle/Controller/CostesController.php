<?php

namespace Iweb\FactuBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Iweb\FactuBundle\Entity\FacturaRecibida as Factura;
use Iweb\FactuBundle\Form\FacturaType;
use Symfony\Component\HttpFoundation\Response;

/**
 * Factura controller.
 *
 * @Route("/facturacion/costes")
 */
class CostesController extends Controller
{

    /**
     * @Route("/", name="costes_listado")
     */
    public function indexAction(Request $request)
    {
        //$facturas = $Empresa = $this->getDoctrine()->getRepository('IwebFactuBundle:Factura')->findAll();
        $trimestre = $request->query->get('trimestre');
        $ano = $request->query->get('ano');
        $cliente = $request->query->get('cliente');
        $queryBuilder = $this->getDoctrine()->getManager()->createQueryBuilder();
        $query = $queryBuilder
                ->select('f')
                ->from(Factura::class, 'f');

        if (!$this->isGranted('ROLE_RG_GLOBAL')) {
            $query->where("f.receptor = :empresa");
            if ($this->isGranted('ROLE_RG_FINANCIERO')) {
                $query->where("f.emisor = :empresa");
            }
            $query->setParameter("empresa", $this->getUser()->getEmpresa());
        }

        if (!is_null($ano) && strlen($ano) == 4) {
            switch ($trimestre) {
                case '1T':
                    $fini = \DateTime::createFromFormat("d-m-Y", "00-01-" . $ano);
                    $ffin = \DateTime::createFromFormat("d-m-Y", "31-03-" . $ano);
                    break;
                case '2T':
                    $fini = \DateTime::createFromFormat("d-m-Y", "00-04-" . $ano);
                    $ffin = \DateTime::createFromFormat("d-m-Y", "30-06-" . $ano);
                    break;
                case '3T':
                    $fini = \DateTime::createFromFormat("d-m-Y", "00-07-" . $ano);
                    $ffin = \DateTime::createFromFormat("d-m-Y", "30-09-" . $ano);
                    break;
                case '4T':
                    $fini = \DateTime::createFromFormat("d-m-Y", "00-10-" . $ano);
                    $ffin = \DateTime::createFromFormat("d-m-Y", "31-12-" . $ano);
                    break;
                default:
                    $fini = \DateTime::createFromFormat("d-m-Y", "00-01-" . $ano);
                    $ffin = \DateTime::createFromFormat("d-m-Y", "31-12-" . $ano);
                    break;
            }
            $query->andwhere('f.fechaEmision >= :fini AND f.fechaEmision <= :ffin');
            $query->setParameter("fini", $fini)
                    ->setParameter("ffin", $ffin);
        }
        if ($cliente != null) {
            $cliente = $this->getDoctrine()->getRepository('AppBundle:Empresa')->findOneById($cliente);
            if (!is_null($cliente)) {
                $query->andwhere('f.emisor = :cliente')
                        ->setParameter(":cliente", $cliente);
            }
        }
        $facturas = $query->getQuery()->getResult();

        $empresas = $this->getDoctrine()->getRepository('AppBundle:Empresa')->findAll();

        return $this->render('IwebFactuBundle:Costes:listado.html.twig', array('facturas' => $facturas, 'empresas' => $empresas, "filtro" => array(
                        "ano" => $ano,
                        "trimestre" => $trimestre,
                        "empresa" => $cliente
                    ))
        );
    }

    /**
     * @Route("/new", name="costes_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $factura = new Factura();
        $form = $this->createForm('Iweb\FactuBundle\Form\CosteType', $factura);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($factura);
            $em->flush();

            return $this->redirectToRoute('costes_listado');
        }

        return $this->render('IwebFactuBundle:Costes:new.html.twig', array(
                    'factura' => $factura,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Factura entity.
     *
     * @Route("/{id}/edit", name="costes_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Factura $factura)
    {
//        $deleteForm = $this->createDeleteForm($factura);
        $editForm = $this->createForm('Iweb\FactuBundle\Form\CosteType', $factura);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($factura);
            $em->flush();

            return $this->redirectToRoute('costes_listado');
        }

        return $this->render('IwebFactuBundle:Costes:edit.html.twig', array(
                    'factura' => $factura,
                    'form' => $editForm->createView()
        ));
    }

    /**
     * Displays a form to edit an existing Factura entity.
     *
     * @Route("/{factura}", name="costes_view")
     * @Method({"GET","POST"})
     */
    public function viewAction(Request $request, Factura $factura)
    {
        if ($request->request->get('notas') != null) {
            $factura->setNotas($request->request->get('notas'));
            $em = $this->getDoctrine()->getManager();
            $em->persist($factura);
            $em->flush();
        }
        return $this->render('IwebFactuBundle:Costes:view.html.twig', array(
                    'factura' => $factura,
        ));
    }

    /**
     *
     * @Route("/{id}/estado", name="costes_estado")
     * @Method("GET")
     */
    public function estadoAction(Factura $factura)
    {
        $em = $this->getDoctrine()->getManager();

        $epagada = $this->getDoctrine()->getRepository(\Iweb\FactuBundle\Entity\Estado::class)->findOneByNombre("PAGADA");
        $efacturada = $this->getDoctrine()->getRepository(\Iweb\FactuBundle\Entity\Estado::class)->findOneByNombre("FACTURADA");
        $ereembolso = $this->getDoctrine()->getRepository(\Iweb\FactuBundle\Entity\Estado::class)->findOneByNombre("REEMBOLSO");

        if ($factura->getEstado() == $epagada) {
            $factura->setEstado($ereembolso);
        } elseif ($factura->getEstado() == $efacturada) {
            $factura->setEstado($epagada);
        } elseif ($factura->getEstado() == $ereembolso) {
            $factura->setEstado($efacturada);
        }
        $em->persist($factura);
        $em->flush();
        return $this->redirectToRoute('costes_listado');
    }

    /**
     *
     * @Route("/{id}/borrar", name="costes_borrar")
     * @Method("GET")
     */
    public function borrarAction(Factura $factura)
    {
        $efacturada = $this->getDoctrine()->getRepository(\Iweb\FactuBundle\Entity\Estado::class)->findOneByNombre("FACTURADA");
        if ($factura->getEstado() == $efacturada) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($factura);
            $em->flush();
        }
        return $this->redirectToRoute('costes_listado');
    }

    /**
     * Displays a form to edit an existing Factura entity.
     *
     * @Route("/{factura}/fichero", name="costes_fichero_add")
     * @Method({"POST"})
     */
    public function upFileAction(Request $request, Factura $factura)
    {
        $em = $this->getDoctrine()->getManager();

        foreach ($request->files->all() as $file) {
            $fichero = new \Iweb\FilesBundle\Entity\Fichero();
            $fichero->upload($file, __DIR__ . '/../../../../web/uploads/');
            $fichero->setEntidad(Factura::class);
            $fichero->setIdEntidad($factura->getId());
            $em->persist($fichero);
            $factura->addFichero($fichero);
        }
        $em->persist($factura);
        $em->flush();

        return new \Symfony\Component\HttpFoundation\JsonResponse("OK", 201);
    }

    /**
     * Delete fichero.
     *
     * @Route("/{factura}/fichero/{fichero}/remove", name="costes_fichero_remove")
     */
    public function ficheroRemoveAction(Request $request, Factura $factura, \Iweb\FilesBundle\Entity\Fichero $fichero)
    {
        $em = $this->getDoctrine()->getManager();
        if ($factura->removeFichero($fichero)) {
            $em->remove($fichero);
            $em->persist($factura);
        }
        $em->flush();
        return $this->redirectToRoute('costes_view', ['factura' => $factura->getId()]);
    }

}
