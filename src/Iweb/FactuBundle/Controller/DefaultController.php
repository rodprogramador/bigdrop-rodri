<?php

namespace Iweb\FactuBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Factura controller.
 *
 * @Route("/facturacion")
 */
class DefaultController extends Controller
{

    /**
     * @Route("/", name="facturacion_index")
     */
    public function indexAction(Request $request)
    {
        //$facturas = $Empresa = $this->getDoctrine()->getRepository('IwebFactuBundle:Factura')->findAll();
        $trimestre = $request->query->get('trimestre');
        $ano = $request->query->get('ano');
        $cliente = $request->query->get('cliente');
        $queryBuilder = $this->getDoctrine()->getManager()->createQueryBuilder();
        $query = $queryBuilder
                ->select('f')
                ->from('IwebFactuBundle:Factura', 'f');

        if (!$this->isGranted('ROLE_RG_GLOBAL')) {
            $query->where("f.receptor = :empresa");
            if ($this->isGranted('ROLE_RG_FINANCIERO')) {
                $query->where("f.emisor = :empresa");
            }
            $query->setParameter("empresa", $this->getUser()->getEmpresa());
        }

        if (!is_null($ano) && strlen($ano) == 4) {
            switch ($trimestre) {
                case '1T':
                    $fini = \DateTime::createFromFormat("d-m-Y", "00-01-" . $ano);
                    $ffin = \DateTime::createFromFormat("d-m-Y", "31-03-" . $ano);
                    break;
                case '2T':
                    $fini = \DateTime::createFromFormat("d-m-Y", "00-04-" . $ano);
                    $ffin = \DateTime::createFromFormat("d-m-Y", "30-06-" . $ano);
                    break;
                case '3T':
                    $fini = \DateTime::createFromFormat("d-m-Y", "00-07-" . $ano);
                    $ffin = \DateTime::createFromFormat("d-m-Y", "30-09-" . $ano);
                    break;
                case '4T':
                    $fini = \DateTime::createFromFormat("d-m-Y", "00-10-" . $ano);
                    $ffin = \DateTime::createFromFormat("d-m-Y", "31-12-" . $ano);
                    break;
                default:
                    $fini = \DateTime::createFromFormat("d-m-Y", "00-01-" . $ano);
                    $ffin = \DateTime::createFromFormat("d-m-Y", "31-12-" . $ano);
                    break;
            }
            $query->andwhere('f.fechaEmision >= :fini AND f.fechaEmision <= :ffin');
            $query->setParameter("fini", $fini)
                    ->setParameter("ffin", $ffin);
        }
        if ($cliente != null) {
            $cliente = $this->getDoctrine()->getRepository('AppBundle:Empresa')->findOneById($cliente);
            if (!is_null($cliente)) {
                $query->andwhere('f.receptor = :cliente')
                        ->setParameter(":cliente", $cliente);
            }
        }
        $facturas = $query->getQuery()->getResult();

        $empresas = $this->getDoctrine()->getRepository('AppBundle:Empresa')->findAll();

        return $this->render('IwebFactuBundle:Default:index.html.twig', array('facturas' => $facturas, 'empresas' => $empresas, "filtro" => array(
                        "ano" => $ano,
                        "trimestre" => $trimestre,
                        "empresa" => $cliente
                    ))
        );
    }

    /**
     * @Route("/new", name="facturacion_new")
     * @Route("/new/{idcliente}/{idreserva}", name="facturacion_new_simple")
     */
    public function newAction(Request $request, $idcliente = null, $idreserva = null)
    {
        //Obtenermos las reservas pedientes de facturar.
        $reservas = $request->request->get('reservas');
        $fecha = $request->request->get('fecha');

        if (is_null($idreserva) && !is_null($reservas)) {
            foreach ($reservas as $idcliente => $reservascli) {
                $this->generarFactura($idcliente, $reservascli, $fecha);
            }
            return $this->redirectToRoute('facturacion_index');
        }

        if (is_null($idreserva) && is_null($reservas)) {
            $queryBuilder = $this->getDoctrine()->getManager()->createQueryBuilder();
            /* @var $queryBuilder \Doctrine\ORM\QueryBuilder */
            $query = $queryBuilder
                    ->select('r')
                    ->from(\AppBundle\Entity\Reserva::class, 'r')
                    ->where("r.factura is NULL ")
                    ->join("r.estado", "e")
                    ->andWhere("e.activa = 1 OR e.nombre = 'FINALIZADA' ")
//                    ->setParameter(":act", true)
//                    ->setParameter(":efin", "FINALIZADA")
                    ->orderBy("r.id")
                    ->orderBy("r.empresa")
            ;

            $activas = $query->getQuery()->getResult();
            return $this->render('IwebFactuBundle:Default:new.html.twig', array("activas" => $activas));
        }

        if (!is_null($idreserva)) {
            $idfactura = $this->generarFactura($idcliente, array($idreserva), $fecha);
            return $this->redirectToRoute('factura_print', array('id' => $idfactura));
        }

        return $this->redirectToRoute('facturacion_index');
    }

    public function generarFactura($idcliente, $pedidos, $fecha = null)
    {
        $Empresa = $this->getDoctrine()->getRepository(\AppBundle\Entity\Empresa::class)->find($idcliente);
        if (!$Empresa) {
            throw $this->createNotFoundException('Empresa no encontrada con el ID ' . $idcliente);
        }
        if (is_null($fecha)) {
            $fecha = date("Y-m-d");
        }
        $EmprePropia = $this->getDoctrine()->getRepository(\AppBundle\Entity\Empresa::class)->findOneByNombre("RG Spain");
        $Factura = new \Iweb\FactuBundle\Entity\Factura();
        $Factura->setEmisor($EmprePropia);
        $Factura->setReceptor($Empresa);
        $Factura->setFechaEmision($fecha);
        $em = $this->getDoctrine()->getManager();
        foreach ($pedidos as $idreserva) {
            /* @var $Reserva \AppBundle\Entity\Reserva */
            $Reserva = $em->getRepository(\AppBundle\Entity\Reserva::class)->find($idreserva);
            if (!$Reserva) {
                throw $this->createNotFoundException('Reserva no encontrada con el ID ' . $idreserva);
            }
            if ($Reserva->getFactura() !== null) {
                throw $this->createNotFoundException('Reserva ya facturada ' . $idreserva . ' - Factura:' . $Reserva->getFactura());
            }
            if ($Reserva->getEntrega()->getEstado()->getNombre() == "FINALIZADO" && $Reserva->getRecogida()->getEstado()->getNombre() == "FINALIZADO") {
                $erFinalizada = $em->getRepository(\AppBundle\Entity\Estado\Reserva::class)->findOneByNombre("FINALIZADA");
                $Reserva->setEstado($erFinalizada);
            }
            $Reserva->setFactura($Factura);
            $Factura->addPedido($Reserva);
            $em->persist($Reserva);
        }
        $ef_facturada = $em->getRepository(\Iweb\FactuBundle\Entity\Estado::class)->findOneByNombre("FACTURADA");
        $Factura->setEstado($ef_facturada);
        $ultima = $this->get('facturacion.factura')->ultima(\DateTime::createFromFormat("Y-m-d", $fecha)->format("Y"));
        $Factura->setNumero(($ultima));
        $em->persist($Factura);
        $em->flush();
        return $Factura->getId();
    }

}
