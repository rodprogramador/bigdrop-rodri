<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Iweb\FactuBundle\EventListener;

use Doctrine\ORM\EntityManager;

class FacturaListener
{
    protected $em;

    public function __construct(EntityManager $em=null)
    {
        if(!is_null($em)){
            $this->em = $em;
        }
    }
    
    protected function em(){
        if(is_null($this->em)){
            $this->em = $this->getDoctrine()->getManager();
        }
        return $this->em;
    }
    
    public function ultima($ano){
            //$queryBuilder = $this->em()->createQueryBuilder();

            $query = $this->em()->createQuery('
SELECT f1.numero+1 AS inicio, MIN(f2.numero) - 1 AS fin
FROM IwebFactuBundle:Factura f1, IwebFactuBundle:Factura f2
WHERE  f1.numero < f2.numero
AND f1.fechaEmision BETWEEN \''.$ano.'-01-01\' AND \''.$ano.'-12-31\'
AND f2.fechaEmision BETWEEN \''.$ano.'-01-01\' AND \''.$ano.'-12-31\'
GROUP  BY f1.numero
HAVING inicio < MIN(f2.numero)
 ');
            $res = $query->getResult();
            if(count($res) >= 1){
                return $res[0]['inicio'];
            }
            $query = $this->em()->createQuery('
SELECT MAX(f1.numero)+1 AS nueva
FROM IwebFactuBundle:Factura f1
WHERE  f1.fechaEmision BETWEEN \''.$ano.'-01-01\' AND \''.$ano.'-12-31\'
ORDER BY f1.numero DESC
 ');
            $res = $query->getResult();
            if($res[0]['nueva'] == null){
                return 1;
            }else{
                return $res[0]['nueva'];
            }
            
//            $query = $queryBuilder
//                    ->select('MAX(f.numero)')
//                    ->from('IwebFactuBundle:Factura', 'f')
//                    ->where('f.fechaEmision >= \''.$ano.'-01-01\' AND f.fechaEmision <= \''.$ano.'-31-12\'')
//                    ->andwhere('NOT EXISTS( SELECT 1 FROM IwebFactuBundle:Factura f2 WHERE (f2.numero = f.numero + 1) )');
//            $facturas = $query->getQuery()->getResult();
//            if(empty($facturas[0])){
//                return 0;
//            }
//            return $facturas[0][1];
    }
    
}