<?php

namespace Iweb\FactuBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Factura
 *
 * @ORM\Table(name="factura")
 * @ORM\Entity(repositoryClass="Iweb\FactuBundle\Repository\FacturaRepository")
 * @ORM\HasLifecycleCallbacks()

 */
class Factura
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="numero", type="integer")
     */
    private $numero;

    /**
     * @var datetime
     *
     * @ORM\Column(name="fecha_creacion", type="datetime")
     */
    private $fechaCreacion;

    /**
     * @var datetime
     *
     * @ORM\Column(name="fecha_emision", type="datetime")
     */
    private $fechaEmision;

    /**
     * @var Estado
     *
     * @ORM\ManyToOne(targetEntity="Estado")
     * @ORM\JoinColumn(name="estadoId", referencedColumnName="id")
     */
    private $estado;

    /**
     * @var int
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Reserva", mappedBy="factura")
     *
     */
    private $pedido;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Empresa")
     */
    private $emisor;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Empresa")
     */
    private $receptor;

    public function __construct()
    {
        $this->fechaCreacion = new \DateTime();
        $this->fechaEmision = new \DateTime();
        $this->pedido = new \Doctrine\Common\Collections\ArrayCollection();
    }

//    /**
//    * @ORM\PrePersist
//    * @ORM\PreUpdate
//    */
//    public function prePersist(){
//        if($this->id == null){
//            $ultima = $this->get('facturacion.factura')->ultimo();
//            $this->numero = $ultima+1;
//        }
//    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    function getFechaEmision()
    {
        return $this->fechaEmision;
    }

    function getEstado()
    {
        return $this->estado;
    }

    function getEmisor()
    {
        return $this->emisor;
    }

    function getReceptor()
    {
        return $this->receptor;
    }

    /**
     *
     * @return \AppBundle\Entity\Reserva[]
     */
    function getPedido()
    {
        return $this->pedido;
    }

    function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = \DateTime::createFromFormat("Y-m-d", $fechaCreacion);
        return $this;
    }

    function setFechaEmision($fechaEmision)
    {
        $this->fechaEmision = \DateTime::createFromFormat("Y-m-d", $fechaEmision);
        return $this;
    }

    function setEstado($estado)
    {
        $this->estado = $estado;
        return $this;
    }

    function setEmisor($emisor)
    {
        $this->emisor = $emisor;
        return $this;
    }

    function setNumero($numero)
    {
        $this->numero = $numero;
        return $this;
    }

    function setReceptor($receptor)
    {
        $this->receptor = $receptor;
        return $this;
    }

    public function getNumero()
    {
        return $this->numero;
    }

    function addPedido(\AppBundle\Entity\Reserva $pedido)
    {
        $this->pedido[] = $pedido;
        return $this;
    }

    function getTotal()
    {
        $subtotal = 0;
        foreach ($this->pedido as $pedido) {
            $subtotal += $pedido->getTotal();
        }
        return $subtotal;
    }

    function getTotalBruto()
    {
        $subtotal = 0;
        foreach ($this->pedido as $pedido) {
            $subtotal += $pedido->getTotalBruto();
        }
        return $subtotal;
    }

    function getImpuestos()
    {
        return $this->pedido[0]->getPrecioImpuestos();
    }

    public function getTotalImpuestos()
    {
        return $this->getTotal() - $this->getTotalBruto();
    }

}
