<?php

namespace Iweb\FactuBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Factura
 *
 * @ORM\Entity(repositoryClass="Iweb\FactuBundle\Repository\FacturaRecibidaRepository")
 * @ORM\HasLifecycleCallbacks()

 */
class FacturaRecibida
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="numero", type="string", nullable=true)
     */
    private $numero;

    /**
     * @var int
     *
     * @ORM\Column(name="categoria", type="string")
     */
    private $categoria;

    /**
     * @var datetime
     *
     * @ORM\Column(name="fecha_creacion", type="datetime")
     */
    private $fechaCreacion;

    /**
     * @var datetime
     *
     * @ORM\Column(name="fecha_emision", type="datetime")
     */
    private $fechaEmision;

    /**
     * @var datetime
     *
     * @ORM\Column(name="fecha_vencimiento", type="datetime", nullable=true)
     */
    private $fechaVencimiento;

    /**
     * @var datetime
     *
     * @ORM\Column(name="fecha_pago", type="datetime", nullable=true)
     */
    private $fechaPago;

    /**
     * @var datetime
     *
     * @ORM\Column(name="alerta", type="integer")
     */
    private $alerta = 0;

    /**
     * @var Estado
     *
     * @ORM\ManyToOne(targetEntity="Estado")
     * @ORM\JoinColumn(name="estadoId", referencedColumnName="id")
     */
    private $estado;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Empresa")
     */
    private $emisor;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Empresa")
     */
    private $receptor;

    /**
     * @var float
     *
     * @ORM\Column(name="base", type="float")
     */
    private $base;

    /**
     * @var int
     *
     * @ORM\Column(name="impuestos", type="integer")
     */
    private $impuestos;

    /**
     * @var string
     *
     * @ORM\Column(name="notas", type="text", nullable=true)
     */
    private $notas;

    /**
     *
     * @ORM\Column(name="ficheros", type="array", nullable=true)
     */
    private $ficheros;

    public function __construct()
    {
        $this->fechaCreacion = new \DateTime();
        $this->fechaEmision = new \DateTime();
        $this->fechaVencimiento = new \DateTime();
    }

    /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
        if ($this->getFechaVencimiento() < $this->getFechaEmision()) {
            $this->setFechaVencimiento($this->getFechaEmision());
        }
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    function getFechaEmision()
    {
        return $this->fechaEmision;
    }

    function getEstado()
    {
        return $this->estado;
    }

    function getEmisor()
    {
        return $this->emisor;
    }

    function getReceptor()
    {
        return $this->receptor;
    }

    function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = \DateTime::createFromFormat("Y-m-d", $fechaCreacion);
        return $this;
    }

    function setFechaEmision($fechaEmision)
    {
        if ($fechaEmision instanceof \DateTime) {
            $this->fechaEmision = $fechaEmision;
        } else {
            $this->fechaEmision = \DateTime::createFromFormat("Y-m-d", $fechaEmision);
        }
        return $this;
    }

    function setEstado($estado)
    {
        $this->estado = $estado;
        return $this;
    }

    function setEmisor($emisor)
    {
        $this->emisor = $emisor;
        return $this;
    }

    function setNumero($numero)
    {
        $this->numero = $numero;
        return $this;
    }

    function setReceptor($receptor)
    {
        $this->receptor = $receptor;
        return $this;
    }

    public function getNumero()
    {
        return $this->numero;
    }

    function getTotal()
    {
        return $this->getBase() + $this->getTotalImpuestos();
    }

    function getTotalBruto()
    {
        return $this->getBase();
    }

    function getImpuestos()
    {
        return $this->impuestos;
    }

    public function getTotalImpuestos()
    {
        return $this->getBase() * ($this->getImpuestos() / 100);
    }

    public function getBase()
    {
        return $this->base;
    }

    public function getNotas()
    {
        return $this->notas;
    }

    public function getFicheros()
    {
        return $this->ficheros;
    }

    public function setBase($base)
    {
        $this->base = $base;
        return $this;
    }

    public function setNotas($notas)
    {
        $this->notas = $notas;
        return $this;
    }

    public function addFichero($fichero)
    {
        $this->ficheros[] = $fichero;
        return $this;
    }

    public function removeFichero($fichero)
    {
        foreach ($this->ficheros as $k => $file) {
            if ($file->getId() == $fichero->getId()) {
                unset($this->ficheros[$k]);
                return true;
            }
        }
        return false;
    }

    public function setImpuestos($impuestos)
    {
        $this->impuestos = $impuestos;
        return $this;
    }

    public function getCategoria()
    {
        return $this->categoria;
    }

    public function getFechaVencimiento()
    {
        return $this->fechaVencimiento;
    }

    public function getFechaPago()
    {
        return $this->fechaPago;
    }

    public function getAlerta()
    {
        return $this->alerta;
    }

    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;
        return $this;
    }

    public function setFechaVencimiento($fechaVencimiento)
    {
        $this->fechaVencimiento = $fechaVencimiento;
        return $this;
    }

    public function setFechaPago($fechaPago)
    {
        $this->fechaPago = $fechaPago;
        return $this;
    }

    public function setAlerta($alerta)
    {
        $this->alerta = $alerta;
        return $this;
    }

}
